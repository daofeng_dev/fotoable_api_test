import logging

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from api_test.common.api_response import JsonResponse
from api_test.models import UserAccount
from api_test.serializers import UserAccountSerializer, UserAccountDeserializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class UserAccountListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="用户名"),
        openapi.Parameter(name="owner", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="拥有人"),
        openapi.Parameter(name="page", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="当前页数"),
        openapi.Parameter(name="page_size", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description="每页最多显示的用户账号数"),
    ])
    def get(self, request):
        """
        获取测试用户列表
        """
        try:
            page_size = int(request.GET.get("page_size", 10))
            page = int(request.GET.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999995", msg="page and page_size must be integer！")
        name = request.GET.get("name")
        owner = request.GET.get("owner")
        obi = UserAccount.objects.all()
        if name:
            obi = obi.filter(name=name)
        if owner:
            obi = obi.filter(owner=owner)
        obi = obi.order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.count
        try:
            obm = paginator.page(page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = UserAccountDeserializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": page,
                                  "page_sizes": page_size,
                                  "total": total
                                  }, code="999999", msg="成功！")


class AddUserAccountView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name'],
        properties={
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="用户名"),
            'loginPwd': openapi.Schema(type=openapi.TYPE_STRING, description="登录密码"),
            'payPwd': openapi.Schema(type=openapi.TYPE_STRING, description="支付密码"),
            'owner': openapi.Schema(type=openapi.TYPE_STRING, description="拥有人"),
            'identify': openapi.Schema(type=openapi.TYPE_STRING, description="用户身份"),
            'money': openapi.Schema(type=openapi.TYPE_STRING, description="账户余额"),
            'desc': openapi.Schema(type=openapi.TYPE_STRING, description="备注")
        },
    ))
    def post(self, request):
        """
        新增测试用户账号
        """
        data = JSONParser().parse(request)
        name = data.get('name')
        if not name:
            return JsonResponse(code="999995", msg="参数name缺失！")
        obi = UserAccount.objects.filter(name=name)
        if obi:
            return JsonResponse(code="999997", msg="存在相同名称！")
        else:
            serializer = UserAccountSerializer(data=data)
            with transaction.atomic():
                if serializer.is_valid():
                    serializer.save()
                    # 记录动态
                    return JsonResponse(data={
                        "host_id": serializer.data.get("id")
                    }, code="999999", msg="新增成功！")
                return JsonResponse(code="999998", msg="失败！")


class UpdateUserAccountView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['name', 'id']:
            if not data.get(param):
                return JsonResponse(code="999995", msg=f"参数{param}缺失！")
        if not isinstance(data.get('id'), int):
            return JsonResponse(code="999995", msg="参数id格式不为int类型！")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name', 'id'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="用户id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="用户名"),
            'loginPwd': openapi.Schema(type=openapi.TYPE_STRING, description="登录密码"),
            'payPwd': openapi.Schema(type=openapi.TYPE_STRING, description="支付密码"),
            'owner': openapi.Schema(type=openapi.TYPE_STRING, description="拥有人"),
            'identify': openapi.Schema(type=openapi.TYPE_STRING, description="用户身份"),
            'money': openapi.Schema(type=openapi.TYPE_STRING, description="账户余额"),
            'desc': openapi.Schema(type=openapi.TYPE_STRING, description="备注")
        },
    ))
    def post(self, request):
        """
        修改测试用户账号
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        aid = data.get('id')
        name = data.get('name')
        obi = UserAccount.objects.filter(id=aid)
        if not obi:
            return JsonResponse(code="999995", msg=f"id为{aid}记录不存在!")
        account_name = UserAccount.objects.filter(name=name).exclude(id=aid)
        if len(account_name):
            return JsonResponse(code="999997", msg="存在相同名称！")
        else:
            serializer = UserAccountSerializer(data=data)
            with transaction.atomic():
                if serializer.is_valid():
                    serializer.update(instance=obi[0], validated_data=data)
                    return JsonResponse(code="999999", msg="更新成功!")
                else:
                    return JsonResponse(code="999998", msg="更新失败!", data=serializer.errors)
