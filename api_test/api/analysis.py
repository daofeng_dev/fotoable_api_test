import calendar
import logging
from datetime import date, timedelta, datetime

import jenkins
from dateutil.relativedelta import relativedelta
from django.db.models import Q, Count, Sum
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView

from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import Demand, ApiInfo, ApiCaseInfo, Project, DemandReport, CaseInfo, CaseExecuteRecord, \
    LoadCaseFile

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class AnalysisView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_executing_demand_info():
        # 执行中需求id列表
        executing_demand_list = [i.get('id') for i in
                                 Demand.objects.filter(~Q(name__contains='测试平台'), status='进行中', state=True).values(
                                     'id')]
        demand_case_list = CaseInfo.objects.filter(demand_id__in=executing_demand_list).values_list(
            'demand_id').annotate(total=Count("demand_id"))
        result = []
        for i in demand_case_list:
            round_list = []
            case_record = CaseExecuteRecord.objects.filter(demand_id=i[0])
            for k in ['第一轮测试', '第二轮测试', '第三轮测试']:
                record = case_record.filter(round=k)
                success = record.filter(response='通过')
                failed = record.filter(response='失败')
                block = record.filter(response='阻塞')
                round_list.append(dict(success=success.count(), failed=failed.count(), block=block.count(),
                                       total=record.count(),
                                       progress=round(record.values('case_id').distinct().count() / i[1], 4) * 100))
            obj = dict(demand=dict(id=Demand.objects.get(id=i[0]).id, name=Demand.objects.get(id=i[0]).name),
                       total=i[1], round=round_list)
            result.append(obj)
        return result

    @staticmethod
    def get_last_record(name):
        server = jenkins.Jenkins('http://10.31.4.246:8081', username='admin', password='admin')
        res = server.get_job_info(name).get('lastBuild')
        number = res.get('number')
        url = res.get('url') + 'robot/report/report.html'
        result = server.get_build_info(name, number).get('actions')[-2]
        try:
            fail, skip, total = result.get('failCount'), result.get('skipCount'), result.get('totalCount')
            success = total - fail - skip
            passed_rate = round(success / total * 100, 1)
        except TypeError as e:
            print(e.__str__())
            fail, skip, total, passed_rate, success = 0, 0, 0, 0, 0
        return dict(fail=fail, success=success, skip=skip, total=total, url=url, rate=passed_rate)

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="type", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="类型"),
    ])
    @catch_exception
    def get(self, request):
        """
        获取项目分析相关数据
        """
        d_type = request.GET.get("type", 'month')
        now = datetime.now()
        if d_type not in ['day', 'week', 'month', 'year']:
            return JsonResponse(code="999995", msg="参数type枚举类错误！")
        if d_type == 'day':
            start = date.today()
            end = start + timedelta(days=1)
        elif d_type == 'week':
            start = date.today() - timedelta(days=datetime.today().weekday())
            end = date.today() + timedelta(days=7 - date.today().weekday())
        elif d_type == 'month':
            start = request.GET.get("start", datetime(now.year, now.month, 1))
            end = request.GET.get("end", datetime(now.year, now.month, calendar.monthrange(now.year, now.month)[1]))
        elif d_type == 'year':
            start = datetime.today() - relativedelta(year=1)
            end = datetime.today()
        api_count = ApiInfo.objects.filter(status=True).count()
        case_count = ApiCaseInfo.objects.filter(status=True).count()
        result = self.get_last_record('api_autotest')
        demand = Demand.objects.filter(state=True)
        demand_count = demand.count()
        demand_total = demand.filter(
            Q(releaseTime__range=(start, end)) | Q(startTime__range=(start, end)) | Q(endTime__range=(start, end)) | Q(
                status='未开始') | Q(startTime__lt=start, endTime__gt=end))
        not_start_demand = demand.filter(Q(startTime__range=(start, end), status='未开始') | Q(status='未开始'))
        doing_demand = demand.filter(
            Q(startTime__range=(start, end)) | Q(endTime__range=(start, end)) | Q(startTime__lt=start, endTime__gt=end),
            status='进行中')
        done_demand = demand.filter(Q(endTime__range=(start, end)), status='已完成')
        release_demand = demand.filter(status='已上线', releaseTime__range=(start, end))
        day_done_demand = demand.filter(endTime__range=(date.today(), date.today() + timedelta(days=1)), status='已完成')
        year_demand_list = []
        case_rank_list = []
        api_rank_list = []
        process_list = self.get_executing_demand_info()
        demand_rank_list = [dict(id=demand.id, name=demand.name, releaseTime=demand.releaseTime.strftime('%Y-%m-%d'),
                                 status=demand.status) for demand in
                            Demand.objects.filter(Q(releaseTime__isnull=False) & ~Q(name__contains='测试平台'),
                                                  status='已上线').order_by('-releaseTime')[0:10]]
        project_list = [{'name': obj.name, 'id': obj.id} for obj in Project.objects.filter(status=True)]
        report_list = DemandReport.objects.values("type").annotate(total=Count("type"))
        report_total = DemandReport.objects.count()
        demand_case_total = LoadCaseFile.objects.aggregate(Sum('count'))['count__sum']
        demand_case_total = 0 if not demand_case_total else demand_case_total
        new_report_list = []
        for report in report_list:
            if not report.get('type'):
                report['type'] = '其他'
            report['rate'] = f"{round(report['total'] / report_total * 100, 2)}%"
            new_report_list.append(report)
        for project in project_list:
            total = ApiCaseInfo.objects.filter(status=True, project=project.get('id')).count()
            case_rank_list.append(dict(name=project.get('name'), total=total))
            aip_total = ApiInfo.objects.filter(status=True, project=project.get('id')).count()
            api_rank_list.append(dict(name=project.get('name'), total=aip_total))
        start = datetime.now() - relativedelta(years=1)
        start_time = datetime(year=start.year, month=start.month, day=1)
        count = 0
        for i in range(1, 14):
            start = start_time + relativedelta(months=i - 1)
            end = start_time + relativedelta(months=i)
            month = start.strftime('%Y.%m')
            if i == 13:
                demand_month = Demand.objects.filter(
                    Q(releaseTime__range=(start, end)) | Q(startTime__range=(start, end),
                                                           endTime__range=(start, end)) | Q(status='未开始'),
                    state=True).count()
            else:
                demand_month = Demand.objects.filter(state=True, releaseTime__range=(start, end)).count()
            count += demand_month
            year_demand_list.append({'x': month, 'y': demand_month})
        year_demand_list = [dict(dict(rate=f"{round(item['y'] / count * 100, 2)}%"), **item) for item in year_demand_list]

        demand_set = Demand.objects.filter(~Q(case_id=None)).values('case_id', 'name', 'case__count')
        total = demand_set.aggregate(Sum('case__count'))['case__count__sum']
        demand_case_list = [dict(x=demand['name'], y=demand['case__count'], rate=f"{round(demand['case__count'] / total * 100, 2)}%" if demand['case__count'] > 0 else "0%") for demand in demand_set]
        demand_case_list.sort(key=lambda x: x["y"], reverse=True)
        res = dict(apiNum=api_count, caseNum=case_count, notStart=not_start_demand.count(), doing=doing_demand.count(),
                   done=done_demand.count(), release=release_demand.count(), demandTotal=demand_total.count(),
                   dayDoneNum=day_done_demand.count(), demandNum=demand_count, result=result,
                   projectNum=len(project_list), yearList=year_demand_list, caseRankList=case_rank_list,
                   apiRankList=api_rank_list, reportList=report_list, demandRankList=demand_rank_list,
                   processList=process_list, demandCaseTotal=demand_case_total, demandCaseList=demand_case_list[:10])
        return JsonResponse(data=res, code="999999", msg="查询成功")


class DemandLevelList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="start", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="开始时间"),
        openapi.Parameter(name="end", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="结束时间"),

    ])
    @catch_exception
    def get(self, request):
        """
        获取项目分析相关数据
        """
        now = datetime.now()
        start = request.GET.get("start", datetime(now.year, now.month, 1))
        end = request.GET.get("end", datetime(now.year, now.month, calendar.monthrange(now.year, now.month)[1]))
        demand = Demand.objects.filter(state=True)
        release_demand = demand.filter(status='已上线', releaseTime__range=(start, end))
        level_s1, level_s, level_a1, level_a, level_b = release_demand.filter(level='S+').count(), \
                                                        release_demand.filter(level='S').count(), \
                                                        release_demand.filter(level='A+').count(), \
                                                        release_demand.filter(level='A').count(), \
                                                        release_demand.filter(level__in=['B', '']).count()
        res = dict(levelS1=dict(count=level_s1, rate=f"{round(level_s1 / release_demand.count() * 100, 2)}%" if release_demand.count() > 0 else "0%"),
                   levelS=dict(count=level_s, rate=f"{round(level_s / release_demand.count() * 100, 2)}%" if release_demand.count() > 0 else "0%"),
                   levelA1=dict(count=level_a1, rate=f"{round(level_a1 / release_demand.count() * 100, 2)}%" if release_demand.count() > 0 else "0%"),
                   levelA=dict(count=level_a, rate=f"{round(level_a / release_demand.count() * 100, 2)}%" if release_demand.count() > 0 else "0%"),
                   levelB=dict(count=level_b, rate=f"{round(level_b / release_demand.count() * 100, 2)}%" if release_demand.count() > 0 else "0%"))
        return JsonResponse(data=res, code="999999", msg="查询成功")
