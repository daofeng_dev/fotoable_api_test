import json
import logging
from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator
from django.db import transaction
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import Devices, App, AppAccount
from api_test.serializers import DeviceDeserializer, DeviceSerializer, AppDeserializer, AppSerializer, \
    AppAccountSerializer, AppAccountDeserializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class AddAppPackageView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        验证参数
        """
        # 必传参数
        for param in ["appPackage", "appActivity", "name"]:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空！")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=["appPackage", "appActivity", "name"],
        properties={
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="应用名称"),
            'appPackage': openapi.Schema(type=openapi.TYPE_STRING, description="应用包名"),
            'appActivity': openapi.Schema(type=openapi.TYPE_STRING, description="appActivity"),
        },
    ))
    def post(self, request):
        """
        app应用信息新增
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        data['createTime'] = datetime.now()
        data['lastUpdateTime'] = datetime.now()
        serializer = AppDeserializer(data=data)
        try:
            App.objects.get(name=data["name"])
            return JsonResponse(code="999997", msg="存在相同名称")
        except ObjectDoesNotExist:
            with transaction.atomic():
                if serializer.is_valid():
                    serializer.save()
                    pid = serializer.data.get("id")
                    return JsonResponse(data={
                        "id": pid
                    }, code="999999", msg="app应用信息新增成功")
                else:
                    return JsonResponse(code="999998", msg="app应用新增失败", data={"errmsg": serializer.errors})


class AppLisView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="app名称"),
        openapi.Parameter(name="page", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="当前页数"),
        openapi.Parameter(name="page_size", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description="每页最多显示的host数"),
    ])
    def get(self, request):
        """
        获取app应用信息
        """
        try:
            page_size = int(request.GET.get("page_size", 30))
            current_page = int(request.GET.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999995", msg="page and page_size must be integer！")
        name = request.GET.get("name")
        if name:
            obi = App.objects.filter(name=name.strip())
        else:
            obi = App.objects.all().order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.count  # 总页数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = AppSerializer(obm, many=True)
        result = []
        for data in serialize.data:
            account = AppAccount.objects.filter(app_id=data['id']).all()
            if account:
                data['account'] = AppAccountSerializer(account, many=True).data
            else:
                data['account'] = []
            result.append(data)
        return JsonResponse(data={"data": result, "current_page": current_page, "total": total,
                                  "page_sizes": page_size}, code="999999", msg="查询app应用列表成功")


class EditAppView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        验证参数
        """
        # 必传参数
        for param in ["appPackage", "appActivity", "name", 'id']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空！")
        obj = App.objects.filter(id=data.get('id'))
        if not obj:
            return JsonResponse(code="999995", msg=f"记录{data.get('id')}不存在,请修改参数id!")
        if App.objects.filter(name=data.get('name')).exclude(id=data.get('id')):
            return JsonResponse(code="999995", msg=f"应用名称{data.get('name')}已存在,请修改参数name!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=["appPackage", "appActivity", "name", 'id'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="ID"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="应用名称"),
            'appPackage': openapi.Schema(type=openapi.TYPE_STRING, description="应用包名"),
            'appActivity': openapi.Schema(type=openapi.TYPE_STRING, description="appActivity")
        },
    ))
    @catch_exception
    def post(self, request):
        """
        app应用信息修改
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        obj = App.objects.get(id=data["id"])
        data['lastUpdateTime'] = datetime.now()
        serializer = AppDeserializer(data=data)
        with transaction.atomic():
            if serializer.is_valid():
                serializer.update(instance=obj, validated_data=data)
                return JsonResponse(code="999999", msg="app应用信息编辑成功")
            else:
                return JsonResponse(code="999998", msg="app应用信息编辑失败", data=serializer.errors)


class AppAccountLisView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="appId", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="应用ID"),
        openapi.Parameter(name="page", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="当前页数"),
        openapi.Parameter(name="page_size", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description="每页最多显示的host数"),
    ])
    @catch_exception
    def get(self, request):
        """
        获取应用账号信息
        """
        try:
            page_size = int(request.GET.get("page_size", 30))
            current_page = int(request.GET.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999995", msg="page and page_size must be integer！")
        app_id = request.GET.get('appId')
        if app_id:
            obi = AppAccount.objects.filter(app_id=app_id).all().order_by("id")
        else:
            obi = AppAccount.objects.all().order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = AppAccountSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data, "current_page": current_page, "total": total,
                                  "page_sizes": page_size}, code="999999", msg="查询app应用列表账号成功")


class AddAppAccountView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        验证参数
        """
        # 必传参数
        if 'accountList' not in data:
            return JsonResponse(code="999996", msg="参数accountList缺失！")
        if not isinstance(data['accountList'], list):
            return JsonResponse(code="999996", msg="参数accountList格式错误,应为数组！")
        for i, v in enumerate(data.get('accountList')):
            for param in ["username", "password", "app"]:
                if not v.get(param):
                    return JsonResponse(code="999996", msg=f"第{i+1}条记录参数{param}不能为空！")
            obj = App.objects.filter(id=v.get('app'))
            if not obj:
                return JsonResponse(code="999995", msg=f"应用{data.get('app')}不存在,请修改第{i+1}条记录参数app!")
            if App.objects.filter(desc=data.get('username')).exclude(id=data.get('app')):
                return JsonResponse(code="999995", msg=f"用户名称{data.get('username')}已存在,请修改第{i+1}条记录参数username!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=["username", "password", "app"],
        properties={
            'accountList': openapi.Schema(type=openapi.TYPE_ARRAY, description="账号信息",
                                          items=openapi.Schema(type=openapi.TYPE_OBJECT),
                                          default="[{'username': '用户名称', 'password': '用户密码', 'app': '应用id']")},
    ))
    def post(self, request):
        """
        新增应用账号信息
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        app_id = data['accountList'][0]['app']
        obj = AppAccount.objects.filter(app_id=app_id)
        new_data = []
        for i in data['accountList']:
            del i['id']
            new_data.append(i)
        serializer = AppAccountDeserializer(data=new_data, many=True)
        with transaction.atomic():
            if serializer.is_valid():
                obj.delete()
                serializer.save()
                return JsonResponse(code="999999", msg="应用账号编辑成功")
            else:
                return JsonResponse(code="999998", msg="应用账号编辑失败", data=serializer.errors)
