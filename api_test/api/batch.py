import json
import logging
import os
from datetime import datetime

import pandas as pd
import xlrd2
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from django.http import FileResponse
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.views import APIView

from api_test.common.api_loadfile import import_api
from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import Project, ApiGroupLevelFirst, ApiVersionLevelFirst, \
    LoadFile, Demand, ApiInfo, ApiDataType, TestPlan, CaseLibrary, CaseInfo
from api_test.serializers import LoadFileSerializer, \
    LoadFileDeSerializer, DemandDeserializer, ApiInfoDeserializer, ApiParameterDeserializer, ApiResponseDeserializer, \
    TestPlanDeserializer, CaseLibraryDeserializer, CaseInfoDeserializer, CaseExecuteRecordDeserializer
from dofun_test_center_backend import settings


class ImportFileApi(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 校验project_id, id类型为int
            if not data.get("project_id") or not data.get("apiVersion_id") or not data.get("apiModule_id"):
                return JsonResponse(code="999996", msg="参数有误!")
            if not isinstance(data["project_id"], int) or not isinstance(data["ids"], list) \
                    or not isinstance(data["apiVersion_id"], int) or not isinstance(data["apiModule_id"], int):
                return JsonResponse(code="999996", msg="参数类型有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数未知错误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['project_id', 'ids', 'apiVersion_id', 'apiModule_id', 'protocol', 'apiAddress'],
        properties={
            'project_id': openapi.Schema(type=openapi.TYPE_INTEGER, description="项目id"),
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="待上传文件id列表"),
            'apiModule_id': openapi.Schema(type=openapi.TYPE_INTEGER, description="模块id"),
            'apiVersion_id': openapi.Schema(type=openapi.TYPE_INTEGER, description="版本id"),
            'protocol': openapi.Schema(type=openapi.TYPE_STRING, description="网络协议"),
            'requestType': openapi.Schema(type=openapi.TYPE_STRING, description="请求类型"),
            'apiAddress': openapi.Schema(type=openapi.TYPE_STRING, description="接口路径"),
        },
    ))
    def post(self, request):
        """
        批量导入接口信息pb/json/xml
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        try:
            pro_data = Project.objects.get(id=data["project_id"], status=True)
            if not request.user.is_superuser and pro_data.user.is_superuser:
                return JsonResponse(code="999983", msg="无操作权限！")
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="项目不存在or已禁用!")

        try:
            ApiGroupLevelFirst.objects.get(id=data["apiModule_id"], project=data["project_id"], status=True)
            ApiVersionLevelFirst.objects.get(id=data["apiVersion_id"], project=data["project_id"], status=True)
            # 检测所选文件是否为同一类型的文件 pb支持多个文件，xml支持一个文件
            filetype_check = []
            ids_len = len(data["ids"])
            for i in data["ids"]:
                loadfile = LoadFile.objects.get(id=i, project=data["project_id"], status=True)
                file_name = loadfile.file.name
                file_type = file_name.split('.')[-1]
                if file_type not in filetype_check:
                    filetype_check.append(file_type)
            if len(filetype_check) > 1:
                return JsonResponse(code="999995", msg="所选择的文件类型不同，无法批量导入!")
            if ids_len > 1 and "xml" in filetype_check:
                return JsonResponse(code="999995", msg="选择的xml文件超过1个，目前只支持单个xml文件批量导入!")

        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="模块/版本/文件 不存在or已禁用!")

        try:
            api_ids = import_api(filetype_check[0], data["ids"], data["project_id"], data["apiVersion_id"],
                                 data["apiModule_id"], request.user.pk, data["apiAddress"], data["protocol"],
                                 data["requestType"])
            return JsonResponse(code="999999", msg="成功!", data=api_ids)
        except Exception as e:
            logging.exception(e)
            return JsonResponse(code="999998", msg="失败!")


class FileList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="system", in_=openapi.IN_QUERY, type=openapi.TYPE_BOOLEAN, description="系统模版"),
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="文件名"),
    ])
    def get(self, request):
        """
        文件列表
        """
        try:
            page_size = int(request.GET.get("page_size", 1000))
            page = int(request.GET.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999995", msg="page and page_size must be integer！")

        system = request.GET.get("system")
        name = request.GET.get("name")

        file_load = LoadFile.objects.filter(status=True)
        if system:
            file_load = file_load.filter(system=system)
        if name:
            file_load = file_load.filter(name=name)
        file_load = file_load.order_by("id")

        paginator = Paginator(file_load, page_size)  # paginator对象
        total = paginator.num_pages  # 总页数
        try:
            obm = paginator.page(page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)

        serialize = LoadFileSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": page,
                                  "total": total
                                  }, code="999999", msg="成功!")


class UploadFile(APIView):
    parser_classes = (MultiPartParser, FormParser)
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data, template):
        """
        校验参数
        """
        try:
            # 校验project_id, id类型为int
            if not template or not data:
                return JsonResponse(code="999996", msg="参数有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数未知错误!")

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_FORM, type=openapi.TYPE_STRING, description="待上传的文件名称"),
        openapi.Parameter(name="file", in_=openapi.IN_FORM, type=openapi.TYPE_FILE, description="待上传的文件"),
        openapi.Parameter(name="template", in_=openapi.IN_FORM, type=openapi.TYPE_FILE, description="文件模板"),
    ])
    @catch_exception
    def post(self, request):
        """
        上传单个文件
        """
        data = request.FILES.get('file')
        template = request.data.get('template')
        system = request.data.get('system')
        if system not in ['true', 'false']:
            return JsonResponse(code="999996", msg="参数system不为bool型!")
        else:
            system = True if system == 'true' else False
        desc = request.data.get('desc')
        file_name = data.name
        result = self.parameter_check(data, template)
        if result:
            return result
        # 上传前将本地的文件删除，将数据库相同name的记录删除
        if LoadFile.objects.filter(name=file_name, status=True, system=system):
            return JsonResponse(code="999996", msg="存在相同的文件名!")
        LoadFile.objects.filter(name=file_name, status=True, system=system, template=template).delete()
        file_full_name = os.path.join(settings.MEDIA_ROOT, 'loadfile', file_name)
        if os.path.isfile(file_full_name):
            os.remove(file_full_name)
        params = {'name': file_name, 'file': data, 'template': template, 'system': system, 'desc': desc}
        file_serializer = LoadFileDeSerializer(data=params)
        if file_serializer.is_valid():
            file_serializer.save()
            return JsonResponse(code="999999", msg="上传成功")
        else:
            return JsonResponse(code="999999", msg="上传失败", data=file_serializer.errors)


class DownloadFile(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="文件id"),
        openapi.Parameter(name="template", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="模板类型"),
    ])
    def get(self, request):
        """
        下载单个文件
        """
        fid = request.GET.get("id")
        template = request.GET.get("template")
        if not fid:
            file_load = LoadFile.objects.get(template=template, system=True, status=True)
        else:
            file_load = LoadFile.objects.get(id=fid, status=True)
        file = open(file_load.file.path, 'rb')
        file_name = file_load.file.name.split("/")[1]
        response = FileResponse(file)
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'attachment;filename="%s"' % file_name
        return response


class DeleteFile(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 校验project_id, id类型为int
            if not isinstance(data["ids"], list):
                for i in data["ids"]:
                    if not isinstance(i, int) or not isinstance(int(i), int):
                        return JsonResponse(code="999995", msg="参数有误！")
                return JsonResponse(code="999995", msg="参数有误！")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['ids'],
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="待删除文件id列表"),
        },
    ))
    def post(self, request):
        """
        删除(禁用)上传的文件
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        # 根据项目id和模块 id查找，若存在则删除
        try:
            for j in data["ids"]:
                obj = LoadFile.objects.get(id=j)
                if obj:
                    del_file_name = os.path.join(settings.MEDIA_ROOT, 'loadfile', obj.name)
                    if os.path.isfile(del_file_name):
                        os.remove(del_file_name)
                    obj.status = False
                    obj.save()
            return JsonResponse(code="999999", msg="删除成功！")
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="文件不存在！")
        except Exception as e:
            return JsonResponse(code="999995", msg="出错啦", data=e)


class RunFile(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 校验project_id, id类型为int
            if not isinstance(data["ids"], list):
                for i in data["ids"]:
                    if not isinstance(i, int) or not isinstance(int(i), int):
                        return JsonResponse(code="999995", msg="参数有误！")
                return JsonResponse(code="999995", msg="参数有误！")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['ids'],
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="待删除文件id列表"),
        },
    ))
    @catch_exception
    def post(self, request):
        """
        执行要上传的文件
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        for j in data["ids"]:
            obj = LoadFile.objects.get(id=j)
            if obj:
                file_name_path = os.path.join(settings.MEDIA_ROOT, 'loadfile', obj.name)
                template = obj.template
                if template.upper() == 'DEMAND':
                    # 执行需求模板
                    return self.run_demand(file_name_path)
                elif template.upper() == 'PLAN':
                    # 执行计划模板
                    return self.run_plan(file_name_path, request)
                elif template.upper() == 'API':
                    # 执行接口模板
                    return self.run_api(file_name_path)

    @staticmethod
    def run_plan(file_name, request):
        plan = pd.read_csv(file_name, engine='python', sep=',', encoding='utf-8')
        # [{'name': 'test1', 'desc': '测试批量导入计划'}, {'name': 'test2', 'desc': '测试批量导入计划2'}]
        plan_list = plan.to_dict('records')
        plan_name_list = [i['name'] for i in plan_list]
        plan_obj = TestPlan.objects.filter(name__in=plan_name_list)
        if plan_obj:
            repeated_name = [obj.name for obj in plan_obj]
            return JsonResponse(code='999995', msg=f"执行失败,记录{repeated_name}已存在")
        plan_serializer = TestPlanDeserializer(data=plan_list, many=True)
        if plan_serializer.is_valid():
            plan_serializer.save(createUser=User.objects.get(id=request.user.pk),
                                 updateUser=User.objects.get(id=request.user.pk))
            pid = [i.get("id") for i in plan_serializer.data]
            return JsonResponse(data={
                "id": pid
            }, code="999999", msg="文件执行成功")
        else:
            return JsonResponse(code="999998", msg="文件执行失败", data={"errmsg": plan_serializer.errors})

    @staticmethod
    def run_api(file_name):
        with open(file_name, 'r', encoding='UTF-8') as f:
            # 获取所有的接口信息
            load_dict = json.load(f)
        api_name_list = [i['name'] for i in load_dict]
        repeat_obj = [api.name for api in ApiInfo.objects.filter(name__in=api_name_list, status=True)]
        if len(repeat_obj) > 0:
            return JsonResponse(code="999997", msg=f"接口[{'、'.join(repeat_obj)}]已在列表中存在,请修正数据后执行！")
        success_list = []
        fail_list = []
        for api_info in load_dict:
            # 循环接口信息
            try:
                obj = Project.objects.get(name=api_info["project_name"], status=True)
            except ObjectDoesNotExist:
                fail_list.append(f'接口{api_info["name"]}失败, 项目不存在or已禁用')
                continue
                # return JsonResponse(code="999995", msg="项目不存在or已禁用!")
            # 将project_name转为project_id
            api_info['project_id'] = Project.objects.get(name=api_info["project_name"], status=True).id
            del api_info['project_name']
            api_info['apiModule_id'] = 1
            api_info['apiVersion_id'] = 1
            # 执行错误后，帮助事务回滚
            with transaction.atomic():
                serialize = ApiInfoDeserializer(data=api_info)
                if serialize.is_valid():
                    try:
                        obm = ApiGroupLevelFirst.objects.get(id=api_info["apiModule_id"], status=True)
                        obv = ApiVersionLevelFirst.objects.get(id=api_info["apiVersion_id"], status=True)
                        serialize.save(project=obj, apiModule=obm, apiVersion=obv)
                    except KeyError:
                        serialize.save(project=obj)
                    except ObjectDoesNotExist:
                        fail_list.append(f'{api_info["name"]}失败, 该项目下不存在该模块or版本!')
                        continue
                    api_id = serialize.data.get("id")
                    try:
                        if api_info.get("requestList") and len(api_info.get("requestList")):
                            for parameter in api_info["requestList"]:
                                if parameter.get("name"):
                                    parameter["api"] = api_id
                                    parameter['status'] = 0.23
                                    parameter['type_id'] = ApiDataType.objects.get(type=parameter["type"], status=True).id
                                    del parameter['type']
                                    param_serialize = ApiParameterDeserializer(data=parameter)
                                    if param_serialize.is_valid():
                                        obi_type = ApiDataType.objects.get(id=parameter["type_id"], status=True)
                                        param_serialize.save(api=ApiInfo.objects.get(id=api_id), type=obi_type)
                                    else:
                                        print(f"{api_info['name']}失败,api创建成功,apiParameter创建失败,失败原因:参数requestList错误, {param_serialize.errors}")
                                        raise KeyError(f"{api_info['name']}失败,api创建成功,apiParameter创建失败,失败原因:参数requestList错误")
                    except KeyError:
                        fail_list.append(f"{api_info['name']}失败,api创建成功,apiParameter创建失败,失败原因:参数requestList错误")
                        continue
                    try:
                        if api_info.get("responseList") and len(api_info.get("responseList")):
                            for response_value in api_info["responseList"]:
                                if response_value.get("name"):
                                    response_value["api"] = api_id
                                    response_value["status"] = True
                                    response_value['repeated'] = False
                                    response_value['type_id'] = ApiDataType.objects.get(type=response_value["type"],
                                                                                        status=True).id
                                    del response_value['type']
                                    response_serialize = ApiResponseDeserializer(data=response_value)
                                    if response_serialize.is_valid():
                                        obi_type = ApiDataType.objects.get(id=response_value["type_id"], status=True)
                                        response_serialize.save(api=ApiInfo.objects.get(id=api_id), type=obi_type)
                                    else:
                                        print(f"{api_info['name']}失败,api创建成功,apiResponse创建失败,失败原因:参数responseList错误, {response_serialize.errors}")
                                        raise KeyError(f"{api_info['name']}失败,api创建成功,api创建成功,apiResponse创建失败,失败原因:参数responseList错误")
                    except KeyError:
                        fail_list.append(f"{api_info['name']}失败,api创建成功,apiResponse创建失败,失败原因:参数responseList错误")
                        continue
            success_list.append(api_info['name'])
        if len(fail_list) > 0:
            return JsonResponse(code="999996", msg=f"执行成功的接口：{success_list}, 执行失败的接口：{fail_list}")
        else:
            return JsonResponse(code="999999", msg="成功!")

    @staticmethod
    def run_demand(file_name):
        demand = pd.read_csv(file_name, engine='python', sep='\\|', encoding='utf-8')
        demands = demand.to_dict('records')
        name_list = []
        for i in demands:
            if isinstance(i.get('description'), float):
                i['description'] = ''
            if i.get('releaseTime') == '未上线' or isinstance(i.get('releaseTime'), float):
                i['releaseTime'] = None
            if isinstance(i.get('estimateTime'), str):
                try:
                    i['estimateTime'] = float(i['estimateTime'])
                except ValueError:
                    raise ValueError(f'记录:{i.get("name")}字段estimateTime格式错误')
            else:
                if i.get('releaseTime'):
                    try:
                        i['releaseTime'] = datetime.strptime(i.get('releaseTime').strip(), '%Y.%m.%d').strftime(
                            '%Y-%m-%d')
                    except ValueError:
                        raise ValueError(f'记录:{i.get("name")}字段releaseTime格式错误,请检查.')
                else:
                    i['releaseTime'] = None
            if isinstance(i.get('startTime'), float) and isinstance(i.get('endTime'), float):
                i['startTime'] = i['endTime'] = None
            if i.get('manager'):
                try:
                    i['manager'] = str(i['manager'].split('、'))
                except AttributeError:
                    raise AttributeError(f'记录:{i.get("name")}字段manager格式错误')
            else:
                raise ValueError(f'{i.get("name")}记录manager不存在')
            name_list.append(i.get('name'))
        demand_obj = Demand.objects.filter(name__in=name_list)
        if demand_obj:
            repeated_name = [obj.name for obj in demand_obj]
            return JsonResponse(code='999995', msg=f"执行失败,记录{repeated_name}已存在")
        demand_serializer = DemandDeserializer(data=demands, many=True)
        if demand_serializer.is_valid():
            demand_serializer.save()
            pid = [i.get("id") for i in demand_serializer.data]
            return JsonResponse(data={
                "id": pid
            }, code="999999", msg="文件执行成功")
        else:
            return JsonResponse(code="999998", msg="文件执行失败", data={"errmsg": demand_serializer.errors})


class UploadLibCase(APIView):
    parser_classes = (MultiPartParser, FormParser)
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def post(request):
        data = request.FILES.get('file')
        file_type = data.name.split('.')[1]
        template = request.data.get('template')
        demand = request.data.get('demand')
        contents = []
        if file_type in ['xlsx', 'xls']:
            wb = xlrd2.open_workbook(filename=None, file_contents=data.read())
            table = wb.sheets()[0]
            rows = table.nrows
            try:
                for i in range(rows):  # 从0开始把表头省略则读取表头信息,如果从1开始则直接读取数据
                    content = table.row_values(i)  # 一行的数据
                    contents.append(content)
            except Exception as e:
                return JsonResponse(code="999995", msg="批量导入失败,请检查文件", data=e.__str__())
        elif file_type == 'csv':
            contents = [i.decode(encoding='utf-8').replace('\r\n', '').split('|') for i in data.readlines()]
        df = pd.DataFrame(contents[1:], columns=contents[0])
        df = df.to_dict(orient='records')
        if template != 'caseLib':
            demand_obj = Demand.objects.filter(id=demand, state=True)
            if not demand_obj:
                return JsonResponse(code="999995", msg=f"批量导入失败, 需求{demand}不存在或者已删除!")
        serialize, obj = None, None
        if template == 'caseLib':
            serialize = CaseLibraryDeserializer(data=df, many=True)
            obj = CaseLibrary.objects.filter(name__in=[i.get('name') for i in df])
            if obj:
                return JsonResponse(code="999995", msg=f"批量导入失败,用例标题存在重复记录{[i.name for i in obj]}")
        elif template == 'demandCase':
            if not demand:
                return JsonResponse(code="999995", msg=f"批量导入失败,参数demand缺失!")
            for i in df:
                i.update({'demand': demand})
            serialize = CaseInfoDeserializer(data=df, many=True)
            for i in df:
                obj = CaseInfo.objects.filter(name=i.get('name'), demand__id=i.get('demand'),
                                              client=i.get('client'), status=True)
                if obj:
                    return JsonResponse(code="999995", msg=f"批量导入失败,需求{obj[0].demand_id}下{obj[0].client}端用例{obj[0].name}重复")
        elif template == 'caseResult':
            record = []
            for i in df:
                case = CaseInfo.objects.filter(demand_id=demand, index=i.get('index'))
                if not case:
                    return JsonResponse(code="999995", msg=f"批量导入执行结果失败,需求{demand_obj[0].name}下不存在执行顺序为{i.get('index')}的用例")
                rou = []
                for j in range(1, 4):
                    if not (i.get(f'response{j}') and i.get(f'manager{j}') and i.get(f'env{j}')):
                        return JsonResponse(code="999995", msg=f"批量导入执行结果失败,需求{demand_obj[0].name}下用例编号为{i.get('index')}的参数不能为空!")
                    if j == 1:
                        rou = '一'
                    elif j == 2:
                        rou = '二'
                    elif j == 3:
                        rou = '三'
                    if not i.get(f'time{j}'):
                        create_time = datetime.now()
                    else:
                        try:
                            create_time = datetime.strptime(i.get(f'time{j}'), '%Y-%m-%d %H:%M:%S')
                        except ValueError:
                            try:
                                create_time = datetime.strptime(i.get(f'time{j}'), '%Y-%m-%d')
                            except ValueError:
                                try:
                                    create_time = datetime.strptime(i.get(f'time{j}'), '%Y/%m/%d')
                                except ValueError:
                                    try:
                                        create_time = datetime.strptime(i.get(f'time{j}'), '%Y.%m.%d')
                                    except ValueError:
                                        return JsonResponse(code="999996", msg=f"批量导入结果失败,记录{i}下time{j}格式错误!")

                    record.append(dict(response=i.get(f'response{j}'), result=i.get(f'result{j}'), round=f'第{rou}轮测试',
                                       manager=i.get(f'manager{j}'), case_id=case[0].id, env=i.get(f'env{j}'),
                                       createTime=create_time, demand_id=demand))
            with transaction.atomic():  # 执行错误后，帮助事务回滚
                serialize = CaseExecuteRecordDeserializer(data=record, many=True)
                if serialize.is_valid():
                    serialize.save()
                    return JsonResponse(code="999999", msg=f"批量导入执行结果成功!")
                else:
                    return JsonResponse(code="999996", msg="批量导入结果失败!", data=serialize.errors)
        if serialize.is_valid():
            serialize.save(userUpdate=User.objects.get(username=request.user))
            return JsonResponse(data={
            }, code="999999", msg="批量导入成功")
        else:
            return JsonResponse(code="999995", msg="批量导入失败", data=serialize.errors)
