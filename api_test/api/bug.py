import logging
import os.path
import socket
import calendar
from datetime import date, datetime
from django.db import connection
import pandas as pd
import requests
import json
from dateutil.relativedelta import relativedelta
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Count, Q
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from sqlalchemy import create_engine

from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import Bug, Demand, BugAnalysis, MockServiceInfo
from dofun_test_center_backend import settings
from api_test.serializers import BugAnalysisDeserializer, BugAnalysisSerializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class SyncBugListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_bug_list_by_pid(session, pid):
        url = f"http://zentao.zuhaowan.com.cn/index.php?m=bug&f=browse&productid={pid}&branch=0&browseType=all"
        res = session.get(url)
        return res

    @staticmethod
    def get_bug_list_info(session, pid, header):
        # data = f'fileName={pid}&fileType=csv&encode=utf-8&exportType=all&template=22&exportFields%5B%5D=id&exportFields%5B%5D=module&exportFields%5B%5D=project&exportFields%5B%5D=title&exportFields%5B%5D=keywords&exportFields%5B%5D=severity&exportFields%5B%5D=type&exportFields%5B%5D=status&exportFields%5B%5D=openedBy&exportFields%5B%5D=openedDate&exportFields%5B%5D=resolution&exportFields%5B%5D=resolvedDate&exportFields%5B%5D=resolvedBy&title=%5B%E5%85%AC%E5%85%B1%5D+%E8%87%AA%E5%8A%A8%E5%8C%96'
        data = f'fileName={pid}&fileType=csv&encode=utf-8&exportType=all&template=27&exportFields%5B%5D=id&exportFields%5B%5D=module&exportFields%5B%5D=project&exportFields%5B%5D=title&exportFields%5B%5D=keywords&exportFields%5B%5D=severity&exportFields%5B%5D=type&exportFields%5B%5D=status&exportFields%5B%5D=activatedCount&exportFields%5B%5D=openedBy&exportFields%5B%5D=openedDate&exportFields%5B%5D=resolution&exportFields%5B%5D=resolvedDate&exportFields%5B%5D=resolvedBy&title=%E5%AF%BC%E5%87%BAbug%E6%A8%A1%E6%9D%BF'
        url = f"http://zentao.zuhaowan.com.cn/index.php?m=bug&f=export&productID={pid}&orderBy=id_desc&browseType=all"
        response = session.post(url=url, data=data, headers=header)
        if response.status_code != 200:
            return JsonResponse(code="999995", msg="数据同步失败!", data=response.status_code)
        else:
            contents = response.content.decode(encoding='utf-8').replace('0000-00-00', '').split("\n")[1:-1]
            info = "id,module,project,title,keyword,severity,type,status,activatedCount,openedBy,openedDate,resolution,resolvedDate,resolvedBy" + '\n'
            for content in contents:
                info += content + '\n'
            file = os.path.join(settings.MEDIA_ROOT, 'bug', f'{pid}.csv')
            with open(file, 'w+', encoding='utf-8') as f:
                f.write(info)

    @staticmethod
    def write_to_db(file, d_type):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        host = s.getsockname()[0]
        mysql = f"mysql+pymysql://root:root@{host}:3306/dofun_api_test"
        engine = create_engine(mysql, encoding="utf8")
        df = pd.read_csv(file, index_col=False)
        df['openedDate'] = pd.to_datetime(df['openedDate'])
        df['resolvedDate'] = pd.to_datetime(df['resolvedDate'])
        df.to_sql(
            name='api_test_bug',
            con=engine,
            index=False,
            if_exists=d_type
        )

    @catch_exception
    def get(self, request):
        """
        同步禅道bug记录到数据库中
        """
        session = requests.session()
        header = {
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36",
        }
        data = {
            "account": "admin",
            "password": "admin123456",
            "referer": "http://zentao.zuhaowan.com.cn/index.php?m=my&f=index",
            "verifyRand": "188636377"
        }
        resp = session.post("http://zentao.zuhaowan.com.cn/index.php?m=user&f=login", data=data, headers=header)
        if resp.status_code == 200:
            for pid in [1, 3, 13, 19]:
                res = self.get_bug_list_by_pid(session, pid)
                if res.status_code == 200:
                    header['Content-Type'] = 'application/x-www-form-urlencoded'
                    result = self.get_bug_list_info(session, pid, header)
                    if result:
                        return result
                    else:
                        file = os.path.join(settings.MEDIA_ROOT, 'bug', f'{pid}.csv')
                        if pid == 1:
                            self.write_to_db(file, 'replace')
                        else:
                            self.write_to_db(file, 'append')
                else:
                    return JsonResponse(code="999995", msg="禅道bug列表查询失败!")
                projects = set(
                    [bug.get('project').split("(#")[0] for bug in Bug.objects.filter(keyword=None).values('project')])
                for demand in Demand.objects.all():
                    if demand.name in projects:
                        project = f"{demand.name}(#"
                        Bug.objects.filter(project__contains=project).update(keyword=demand.id)
            return JsonResponse(code="999999", msg="数据同步成功")
        else:
            return JsonResponse(code="999995", msg="禅道用户登录失败!")


class AnalysisBugView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_bug_total_count():
        type_list = []
        start = datetime.now() - relativedelta(years=1)
        start_time = datetime(year=start.year, month=start.month, day=1)
        count = 0
        total = Bug.objects.filter(openedDate__range=(start, datetime.now())).count()
        for i in range(1, 13):
            start = start_time + relativedelta(months=i)
            end = start_time + relativedelta(months=i+1)
            month = start.strftime('%Y.%m')
            if i == 13:
                bug_month = Bug.objects.filter(openedDate__range=(start, end)).count()
            else:
                bug_month = Bug.objects.filter(openedDate__range=(start, end)).count()
            count += bug_month
            type_list.append({'x': month, 'y': bug_month, 'rate': f"{round(bug_month / total  * 100, 2)}%" if bug_month > 0 else "0%"})
        return type_list

    @staticmethod
    def get_pie_bug_list(dtype, keywords, keyword_dict, title):
        bug_list = list(Bug.objects.filter(keyword__in=keywords).values(dtype, 'keyword').annotate(total=Count(dtype)))
        demand_bug_list = []
        for key, value in keyword_dict.items():
            count = Bug.objects.filter(keyword=key).count()
            demand_list = []
            for bug in bug_list:
                if key == bug.get('keyword'):
                    demand_dic = dict()
                    if dtype == 'resolution':
                        demand_dic['x'] = bug.get('resolution') if bug.get('resolution') else '修复中'
                        demand_dic['y'] = Bug.objects.filter(keyword=key, status='激活').count() if not bug.get('resolution') else bug.get('total')
                    else:
                        demand_dic['x'] = bug.get(dtype)
                        demand_dic['y'] = bug.get('total')
                    demand_dic['rate'] = f"{round(demand_dic['y'] / count * 100, 2)}%" if demand_dic['y'] > 0 else "0%"
                    if demand_dic['y'] != 0:
                        demand_list.append(demand_dic)
            demand_list.sort(key=lambda x: x["y"], reverse=True)
            demand_bug_list.append(dict(title=title, data=demand_list, label=value))
        return demand_bug_list

    @staticmethod
    def get_bug_list_by_type(bug_arr, bug_total, dtype):
        data = [dict(x=bug.get(dtype), y=bug['total'],
                                rate=f"{round(bug['total'] / bug_total * 100, 2)}%" if bug['total'] > 0 else "0%") for bug in bug_arr]
        data.sort(key=lambda x: x["y"], reverse=True)
        return data

    @staticmethod
    def get_resolved_top_list_by_type(bug_arr, resolved_bug_arr, dtype, title):
        data = []
        for i in resolved_bug_arr:
            bug_list = []
            for bug in bug_arr:
                bug_dic = dict()
                if i.get('resolvedBy') in bug.values():
                    bug_dic['x'] = bug.get(dtype)
                    bug_dic['y'] = bug.get('total')
                    bug_dic['rate'] = f"{round(bug_dic['y'] / i.get('total') * 100, 2)}%" if bug_dic['y'] > 0 else "0%"
                    if bug_dic['y'] != 0:
                        bug_list.append(bug_dic)
            data.append(dict(label=i.get('resolvedBy'), data=bug_list, title=title))
        return data

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="start", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="开始时间"),
        openapi.Parameter(name="end", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="结束时间"),

    ])
    @catch_exception
    def get(self, request):
        """
        不同维度统计bug
        """
        now = datetime.now()
        start = request.GET.get("start", datetime(now.year, now.month, 1).strftime('%Y-%m-%d'))
        end = request.GET.get("end", datetime(now.year, now.month, calendar.monthrange(now.year, now.month)[1]).strftime('%Y-%m-%d'))
        result = []
        mock = MockServiceInfo.objects.filter(address='/qa/list').first()
        pm = ['李钊', '孙飞飞', '夏坤', '邱玉叶', '牛亚楠', '刘肖肖', '徐雅倩', '余媛', '王晨', '张建豪', '景众仰', '张晓磊', '陈龙']
        if not mock:
            authors = ['韩玉', '常磊磊', '崔君瑞', '金淑静', '段钢鑫', '郭宗政', '马俊涛', '夏旭星', '付亮亮', '谷玉芹', '陈海燕', '袁梦茹', '王博', '张利娜']
        else:
            authors = json.loads(mock.data)
        resolved_bug_arr = list(Bug.objects.filter(resolvedDate__range=(start, end)).exclude(resolvedBy__in=authors+pm).values("resolvedBy").annotate(total=Count("resolvedBy")))
        resolved_total = Bug.objects.filter(resolvedDate__range=(start, end)).count()
        resolved_bug_arr.sort(key=lambda x: x["total"])
        resolved_bug_arr = list(reversed(resolved_bug_arr))
        bug_obj = Bug.objects.filter(openedDate__range=(start, end))
        bug_total = bug_obj.count()
        author_bug_arr = bug_obj.filter(openedBy__in=authors).values('openedBy').annotate(total=Count("openedBy"))
        author_list = self.get_bug_list_by_type(author_bug_arr, bug_total, 'openedBy')
        if len(resolved_bug_arr) >= 15:
            resolved_bug_arr = resolved_bug_arr[:15]
        resolved_list = self.get_bug_list_by_type(resolved_bug_arr, resolved_total, 'resolvedBy')
        severity_bug_arr = bug_obj.values('severity').annotate(total=Count("severity"))
        severity_list = self.get_bug_list_by_type(severity_bug_arr, bug_total, 'severity')
        status_bug_arr = bug_obj.values('status').annotate(total=Count("status"))
        status_list = self.get_bug_list_by_type(status_bug_arr, bug_total, 'status')
        resolution_bug_arr = list(bug_obj.values('resolution').annotate(total=Count("resolution")))
        resolution_list = []
        for obj in resolution_bug_arr:
            if not obj.get('resolution'):
                resolution_bug_arr.remove(obj)
                resolution_bug_arr.append(dict(resolution='修复中', total=bug_obj.filter(status='激活').count()))
            resolution_list = self.get_bug_list_by_type(resolution_bug_arr, bug_total, 'resolution')
        keywords = set(
            [int(bug.keyword) for bug in Bug.objects.filter(openedDate__range=(start, end)) if bug and bug.keyword])
        demand_bug_total = Bug.objects.filter(keyword__in=keywords).count()
        demand_list = []
        keyword_dict = {i['id']: i['name'] for i in list(Demand.objects.filter(id__in=keywords).values('name', 'id'))}
        demand_bug_arr = list(Bug.objects.filter(keyword__in=keywords).values('keyword').annotate(total=Count("keyword")))
        for bug in demand_bug_arr:
            demand_dic = dict()
            demand_dic['x'] = keyword_dict.get(bug.get('keyword'))
            demand_dic['y'] = bug.get('total')
            demand_dic['rate'] = f"{round(demand_dic['y'] / demand_bug_total * 100, 2)}%" if demand_dic['y'] > 0 else "0%"
            if demand_dic['y'] != 0:
                demand_list.append(demand_dic)
        demand_list.sort(key=lambda x: x["y"], reverse=True)
        total_list = self.get_bug_total_count()
        pie_data = list()
        for k, v in dict(resolution='需求Bug解决方案统计', severity='需求Bug严重程度统计', resolvedBy='需求Bug解决人统计').items():
            pie_data.append(dict(title=v, data=self.get_pie_bug_list(k, keywords, keyword_dict, v.split('需求Bug')[-1].replace('统计', '分布'))))
        severity_resolved_bug_arr = list(Bug.objects.filter(resolvedDate__range=(start, end)).exclude(resolvedBy__in=authors+pm).values("resolvedBy", "severity").annotate(total=Count("resolvedBy")))
        severity_resolved_bug_list = self.get_resolved_top_list_by_type(severity_resolved_bug_arr, resolved_bug_arr, 'severity', '严重程度分布')
        pie_data.append(dict(title='Bug严重程度解决人排行Top15统计', data=severity_resolved_bug_list))
        resolution_resolved_bug_arr = list(Bug.objects.filter(resolvedDate__range=(start, end)).exclude(resolvedBy__in=authors+pm).values("resolvedBy", "resolution").annotate(total=Count("resolution")))
        resolution_resolved_bug_list = self.get_resolved_top_list_by_type(resolution_resolved_bug_arr, resolved_bug_arr, 'resolution', '解决方案分布')
        pie_data.append(dict(title='Bug解决方案解决人排行Top15统计', data=resolution_resolved_bug_list))
        top_bug = list()
        bug_efficiency = []
        ineffectiveness_bug = Bug.objects.filter(openedDate__range=(start, end), resolution__in=['重复Bug', '无法重现', '设计如此', '外部原因', '不予解决']).count()
        effective_bug = bug_total - ineffectiveness_bug
        bug_efficiency.append(dict(x='有效Bug', y=effective_bug, rate=f"{round(effective_bug / bug_total * 100, 2)}%" if effective_bug > 0 else "0%"))
        bug_efficiency.append(dict(x='无效Bug', y=ineffectiveness_bug, rate=f"{round(ineffectiveness_bug / bug_total * 100, 2)}%" if ineffectiveness_bug > 0 else "0%"))
        demand_bug_efficiency_list = []
        activated_bug_list = []
        for k, v in keyword_dict.items():
            total = Bug.objects.filter(keyword=k).count()
            data = []
            activated_bug_arr = []
            bugs = Bug.objects.filter(keyword=k).values('activatedCount').annotate(total=Count("activatedCount"))
            for bug in bugs:
                x, y, rate = f'重新激活{bug.get("activatedCount")}次', bug.get('total'), f"{round(bug.get('total') / total * 100, 2)}%" if bug.get('total') > 0 else "0%"
                activated_bug_arr.append(dict(x=x, y=y, rate=rate))
            activated_bug_list.append(dict(label=v, data=activated_bug_arr, title='重新激活次数分布'))
            ineffectiveness_bug = Bug.objects.filter(keyword=k, resolution__in=['重复Bug', '无法重现', '设计如此', '外部原因', '不予解决']).count()
            effective_bug = total - ineffectiveness_bug
            data.append(dict(x='有效Bug', y=effective_bug,
                                       rate=f"{round(effective_bug / total * 100, 2)}%" if effective_bug > 0 else "0%"))
            data.append(dict(x='无效Bug', y=ineffectiveness_bug,
                                       rate=f"{round(ineffectiveness_bug / total * 100, 2)}%" if ineffectiveness_bug > 0 else "0%"))
            demand_bug_efficiency_list.append(dict(label=v, data=data, title='有效率分布'))
        pie_data.append(dict(title='需求Bug有效率统计', data=demand_bug_efficiency_list))
        top_bug.append(dict(title='Bug解决方案分布', data=resolution_resolved_bug_list))
        top_bug.append(dict(title='Bug严重程度分布', data=severity_resolved_bug_list))
        result.append(dict(label='Bug数量统计', data=total_list,  title=f'Bug数量统计({total_list[0]["x"]}至{total_list[-1]["x"]})'))
        result.append(dict(label='Bug状态统计', data=status_list, title='Bug状态统计'))
        result.append(dict(label='Bug解决方案统计', data=resolution_list, title='Bug解决方案统计'))
        result.append(dict(label='Bug提交人统计', data=author_list, title='Bug提交人统计'))
        result.append(dict(label='需求Bug数量统计', data=demand_list, title='需求Bug数量统计'))
        result.append(dict(label='Bug严重程度统计', data=severity_list, title='Bug严重程度统计'))
        result.append(dict(label='Bug有效率统计', data=bug_efficiency, title='Bug有效率统计'))
        result.append(dict(label='Bug解决人排行Top15统计', data=resolved_list, title='Bug解决人排行Top15统计'))
        pie_data.append(dict(title='需求Bug重新激活次数统计', data=activated_bug_list))
        return JsonResponse(code="999999", msg="数据统计成功!",
                            data=dict(chartsData=result, pieData=pie_data, topData=top_bug))


class AnalysisBugSortedView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="page", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="当前页数"),
        openapi.Parameter(name="page_size", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description="每页最多显示的项目数"),
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="名称"),
        openapi.Parameter(name="type", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="统计类型"),
    ])
    @catch_exception
    def get(self, request):
        """
        不同维度统计bug
        """
        name = request.GET.get('name')
        d_type = request.GET.get('type', 'resolved')
        page_size = int(request.GET.get("page_size", 10))
        current_page = int(request.GET.get("page", 1))
        obi = BugAnalysis.objects.filter(type=d_type)
        if name:
            obi = obi.filter(name=name.strip()).order_by("id")
        else:
            obi = obi.order_by("-total")
        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = BugAnalysisSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": current_page,
                                  "total": total,
                                  "page_sizes": page_size,
                                  }, code="999999", msg="数据统计成功")


class AnalysisBugByTypeView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    mock = MockServiceInfo.objects.filter(address='/qa/list').first()
    if not mock:
        authors = ['韩玉', '常磊磊', '崔君瑞', '金淑静', '段钢鑫', '郭宗政', '马俊涛', '夏旭星', '付亮亮', '谷玉芹', '陈海燕', '袁梦茹', '王博', '张利娜']
    else:
        authors = json.loads(mock.data)
    resolved_list = set([bug.get('resolvedBy') for bug in
                         Bug.objects.exclude(resolvedBy__in=authors).filter(~Q(status='激活')).values("resolvedBy")])
    keywords = set([bug.get('keyword') for bug in Bug.objects.exclude(keyword='').values('keyword') if isinstance(bug.get("keyword"), float)])

    @staticmethod
    def get_bug_list_by_type(d_type, names, resolution_dict):
        result_arr = []
        if d_type == 'resolved' and names:
            data = list(
                    Bug.objects.filter(resolvedBy__in=names).values('resolution', 'resolvedBy').annotate(total=Count("resolution")))
            for name in names:
                data_dict = dict(name=name)
                total = 0
                for i in data:
                    if i.get('resolvedBy') == name:
                        data_dict[resolution_dict[i['resolution']]] = i['total']
                        total += i['total']
                data_dict['total'] = total
                data_dict['type'] = d_type
                result_arr.append(data_dict)
        elif d_type == 'opened' and names:
            data = list(
                    Bug.objects.filter(openedBy__in=names).exclude(resolution='').values('resolution', 'openedBy').annotate(total=Count("resolution")))
            xfz_dict = {i.get("openedBy"): i.get("total") for i in (list(Bug.objects.filter(openedBy__in=names, resolution=None).values('openedBy').annotate(total=Count("openedBy"))))}
            for name in names:
                data_dict = dict(name=name)
                total = 0
                for i in data:
                    if i.get('openedBy') == name:
                        resolution = resolution_dict.get(i['resolution'])
                        data_dict[resolution] = i['total']
                        total += i['total']
                data_dict['xfz'] = xfz_dict.get(name, 0)
                data_dict['total'] = total + data_dict['xfz']
                data_dict['type'] = d_type
                result_arr.append(data_dict)
        elif d_type == 'demand' and names:
            data = list(Bug.objects.filter(keyword__in=names).exclude(resolution='').values('resolution', 'keyword').annotate(
                        total=Count("resolution")))
            demand_list = {demand.get('id'): demand.get('name') for demand in Demand.objects.filter(id__in=names).values('id', 'name')}
            xfz_dict = {i.get("keyword"): i.get("total") for i in (list(Bug.objects.filter(keyword__in=names, resolution=None).values('keyword').annotate(total=Count("keyword"))))}
            for name in names:
                data_dict = dict(name=demand_list[name])
                total = 0
                for i in data:
                    if i.get('keyword') == name:
                        resolution = resolution_dict.get(i['resolution'])
                        data_dict[resolution] = i['total']
                        total += i['total']
                data_dict['xfz'] = xfz_dict.get(name, 0)
                data_dict['total'] = total + data_dict['xfz']
                data_dict['type'] = d_type
                result_arr.append(data_dict)
        return result_arr

    @catch_exception
    def get(self, request):
        """
        不同维度统计bug数据存储到数据库中
        """

        resolution_dict = {'无法重现': 'wfcx', '外部原因': 'wbyy', '不予解决': 'byjj', '已解决': 'yjj', '延期处理': 'yqcl', '设计如此': 'sjrc',
                           '重复Bug': 'cfbug', '转为需求': 'zwxq'}
        resolved_list = self.get_bug_list_by_type('resolved', self.resolved_list, resolution_dict)
        open_list = self.get_bug_list_by_type('opened', self.authors, resolution_dict)
        demand_list = self.get_bug_list_by_type('demand', self.keywords, resolution_dict)
        data = resolved_list + open_list + demand_list
        serializer = BugAnalysisDeserializer(data=data, many=True)
        if serializer.is_valid():
            cursor = connection.cursor()
            cursor.execute('TRUNCATE TABLE api_test_buganalysis')
            serializer.save()
            return JsonResponse(code="999999", msg="统计数据更新成功!")
        else:
            return JsonResponse(code="999995", msg="统计数据更新失败!", data=serializer.errors)

