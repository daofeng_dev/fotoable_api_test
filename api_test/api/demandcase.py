import logging
import os

import pandas as pd
import xlrd2
from django.db.models import Q
from django.http import FileResponse
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from django_bulk_update.helper import bulk_update
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from datetime import datetime
from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import CaseInfo, User, Demand, CaseLibrary, CaseExecuteRecord, LoadFile, LoadCaseFile
from api_test.serializers import CaseInfoDeserializer, CaseInfoSerializer, CaseLibrarySerializer, \
    CaseExecuteRecordDeserializer, CaseExecuteRecordSerializer, LoadCaseFileSerializer, LoadCaseFileDeSerializer
from dofun_test_center_backend import settings

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class DemandCaseList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="用例名称"),
        openapi.Parameter(name="client", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="所属终端"),
        openapi.Parameter(name="demand", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="所属需求名称"),
        openapi.Parameter(name="round", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="执行轮次"),
        openapi.Parameter(name="manager", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="执行人")

    ])
    def get(self, request):
        """
        获取需求功能测试用例
        """
        name = request.GET.get("name")
        client = request.GET.get("client")
        demand = request.GET.get("demand")
        rou, manager = request.GET.get("round"), request.GET.get("manager")
        page_size = int(request.GET.get("page_size", 10))
        page = int(request.GET.get("page", 1))
        if not isinstance(page, int) or not isinstance(page_size, int):
            return JsonResponse(code="999996", msg="参数有误!")
        obi = CaseInfo.objects.filter(status=True)
        if demand:
            demand_obj = Demand.objects.filter(name=demand)
            if demand_obj:
                obi = obi.filter(demand_id=demand_obj[0].id)
            else:
                return JsonResponse(data={"data": [], "page_sizes": page_size, "total": 0,
                                          'current_page': 1, 'page_count': 1},
                                    code="999999", msg="查询成功!")
        if client:
            obi = obi.filter(client=client)
        if manager:
            id_list = []
            record_id = [case.get('case_id') for case in CaseExecuteRecord.objects.filter(manager=manager).values('case_id').distinct()]
            for record in record_id:
                if CaseExecuteRecord.objects.filter(case_id=record).order_by('-createTime')[0].manager == manager:
                    id_list.append(record)
            obi = obi.filter(Q(manager=manager) | Q(id__in=id_list))
        if name:
            obi = obi.filter(name__contains=name)
        if rou:
            if rou != '未执行':
                id_list = []
                record_id = [case.get('case_id') for case in CaseExecuteRecord.objects.filter(round=rou).values('case_id').distinct()]
                for record in record_id:
                    if CaseExecuteRecord.objects.filter(case_id=record).order_by('-createTime')[0].round == rou:
                        id_list.append(record)
                obi = obi.filter(id__in=id_list)
            else:
                record_id = [case.get('case_id') for case in
                             CaseExecuteRecord.objects.values('case_id').distinct()]
                obi = obi.exclude(id__in=record_id)
        obi = obi.order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        page_count = paginator.num_pages  # 总页数
        total = paginator.count  # 总数
        try:
            obm = paginator.page(page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = CaseInfoSerializer(obm, many=True)
        data = []
        for i in serialize.data:
            obj = CaseExecuteRecord.objects.filter(case_id=i.get('id')).order_by('-createTime')
            if obj:
                i.update(dict(round=obj[0].round))
            else:
                i.update(dict(round=""))
            data.append(i)
        return JsonResponse(data={"data": data, "page_sizes": page_size, "total": total,
                                  'current_page': page, 'page_count': page_count},
                            code="999999", msg="查询成功!")


class AddDemandCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['name', 'steps', 'stage', 'level', 'client', 'demand']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空!")
        # if not isinstance(data.get('steps'), list):
        #     return JsonResponse(code="999996", msg=f"参数steps格式错误,应为数组类型的字符串!")
        # steps = data.get('steps')
        # for step in steps:
        #     for key in ['step', 'action', 'result']:
        #         if key not in step.keys():
        #             return JsonResponse(code="999996", msg=f"参数steps格式错误,缺少键值{key}")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name', 'steps', 'stage', 'level', 'client', 'demand', 'manager'],
        properties={
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="用例标题"),
            'steps': openapi.Schema(type=openapi.TYPE_STRING, description="预期结果"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="描述"),
            'level': openapi.Schema(type=openapi.TYPE_STRING, description="优先级"),
            'client': openapi.Schema(type=openapi.TYPE_STRING, description="所属终端"),
            'demand': openapi.Schema(type=openapi.TYPE_INTEGER, description="所属需求id"),
            'stage': openapi.Schema(type=openapi.TYPE_STRING, description="适应阶段"),
            'preconditions': openapi.Schema(type=openapi.TYPE_STRING, description="前置条件"),
            'keywords': openapi.Schema(type=openapi.TYPE_STRING, description="关键字"),
            'manager': openapi.Schema(type=openapi.TYPE_STRING, description="执行人"),

        },
    ))
    def post(self, request):
        """
        新增需求测试用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        name, demand, client = data.get('name'), data.get('demand'), data.get('client')
        if not Demand.objects.filter(id=demand):
            return JsonResponse(code="999995", msg=f"id为{demand}的需求不存在!")
        count = CaseInfo.objects.filter(name=name, status=True, demand_id=demand, client=client).count()
        if count > 0:
            return JsonResponse(code="999997", msg="存在相同名称!")
        else:
            with transaction.atomic():  # 执行错误后，帮助事务回滚
                serialize = CaseInfoDeserializer(data=data)
                if serialize.is_valid():
                    serialize.save(userUpdate=User.objects.get(username=request.user))
                    return JsonResponse(code="999999", msg=f"需求新增用例成功!", data={"id": serialize.data.get("id")})
                else:
                    return JsonResponse(code="999996", msg="参数有误!", data=serialize.errors)


class EditDemandCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['id', 'name', 'steps', 'stage', 'level', 'client', 'manager']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空!")
        # if not isinstance(data.get('steps'), list):
        #     return JsonResponse(code="999996", msg=f"参数steps格式错误,应为数组类型的字符串!")
        # steps = data.get('steps')
        # for step in steps:
        #     for key in ['step', 'action', 'result']:
        #         if key not in step.keys():
        #             return JsonResponse(code="999996", msg=f"参数steps格式错误,缺少键值{key}")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'name', 'steps', 'stage', 'level', 'client', 'manager'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="用例id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="用例标题"),
            'steps': openapi.Schema(type=openapi.TYPE_STRING, description="预期结果"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="描述"),
            'level': openapi.Schema(type=openapi.TYPE_STRING, description="优先级"),
            'client': openapi.Schema(type=openapi.TYPE_STRING, description="所属终端"),
            'stage': openapi.Schema(type=openapi.TYPE_STRING, description="适应阶段"),
            'preconditions': openapi.Schema(type=openapi.TYPE_STRING, description="前置条件"),
            'keywords': openapi.Schema(type=openapi.TYPE_STRING, description="关键字"),
            'manager': openapi.Schema(type=openapi.TYPE_STRING, description="执行人"),

        },
    ))
    def post(self, request):
        """
        更新功能测试库用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        # data['steps'] = str(data['steps'])
        name, client = data.get('name'), data.get('client')
        try:
            obi = CaseInfo.objects.get(id=data["id"])
            obi.userUpdate = User.objects.get(username=request.user)
        except ObjectDoesNotExist:
            return JsonResponse(code="999990", msg="case不存在!")
        count = CaseInfo.objects.filter(name=name, status=True, client=client, demand_id=obi.demand_id).exclude(id=data["id"]).count()
        if count > 0:
            return JsonResponse(code="999997", msg=f"需求{obi.demand_id}下{client}模块存在相同用例!")
        with transaction.atomic():  # 执行错误后，帮助事务回滚
            serialize = CaseInfoDeserializer(data=data)
            if serialize.is_valid():
                serialize.update(instance=obi, validated_data=data)
                return JsonResponse(code="999999", msg=f"用例编辑成功!")
            else:
                return JsonResponse(code="999996", msg="用例更新失败!", data=serialize.errors)


class DelDemandCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            if not data.get("ids"):
                return JsonResponse(code="999996", msg="参数有误!")
            if not isinstance(data["ids"], list):
                return JsonResponse(code="999996", msg="参数有误!")
            for i in data["ids"]:
                if not isinstance(i, int):
                    return JsonResponse(code="999996", msg="参数有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['ids'],
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="待删除待用例id列表"),
        },
    ))
    def post(self, request):
        """
        删除需求测试用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        id_list = data.get('ids')
        obj = CaseInfo.objects.filter(id__in=id_list, status=True)
        if len(obj) < len(id_list):
            return JsonResponse(code="999996", msg="请检查ids数据,存在不存在的id或已删除的数据!")
        for i in obj:
            i.status = False
        bulk_update(obj, update_fields=['status'])
        return JsonResponse(code="999999", msg="删除成功!")


class ImportLibCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['ids', 'demand', 'client', 'manager']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}缺失或不能为空")
        if not data.get("ids"):
            return JsonResponse(code="999996", msg="参数有误!")
        if not isinstance(data["ids"], list):
            return JsonResponse(code="999996", msg="参数有误!")
        for i in data["ids"]:
            if not isinstance(i, int):
                return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['ids', 'demand', 'client', 'manager'],
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="用例库待导入用例id列表"),
            'demand': openapi.Schema(type=openapi.TYPE_INTEGER, description="需求id"),
            'client': openapi.Schema(type=openapi.TYPE_STRING, description="所属终端"),
            'manager': openapi.Schema(type=openapi.TYPE_STRING, description="执行人"),

        },
    ))
    def post(self, request):
        """
        导入公共测试用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        id_list = [int(i) for i in data.get('ids')]
        demand, manager, client = data.get('demand'), data.get('manager'), data.get('client')
        demand_obj = Demand.objects.filter(id=demand, state=True)
        if not demand_obj:
            return JsonResponse(code="999996", msg=f"id为{demand}的需求不存在或已删除")
        obj = CaseLibrary.objects.filter(id__in=id_list, status=True)
        if len(obj) < len(id_list):
            return JsonResponse(code="999996", msg="请检查ids数据,存在不存在的id或已删除的数据!")
        serialize = CaseLibrarySerializer(obj, many=True)
        data = [dict(name=i.get('name'), description=i.get('description'), steps=i.get('steps'), level=i.get('level'),
                     demand=demand, stage=i.get('stage'), preconditions=i.get('preconditions'),
                     keywords=i.get('keywords'), client=client, manager=manager) for i in serialize.data]
        name_list = [i.get('name') for i in data]
        obi = CaseInfo.objects.filter(name__in=name_list, demand=demand, client=client)
        if obi:
            repeated_name = [i.name for i in obi]
            return JsonResponse(code="999996", msg=f"需求:{demand_obj[0].name}下所属终端:{client}用例{repeated_name}重复")
        with transaction.atomic():  # 执行错误后，帮助事务回滚
            serialize = CaseInfoDeserializer(data=data, many=True)
            if serialize.is_valid():
                serialize.save(userUpdate=User.objects.get(username=request.user))
                return JsonResponse(code="999999", msg=f"需求用例批量导入成功!")
            else:
                return JsonResponse(code="999996", msg="需求用例批量导入失败!", data=serialize.errors)


class ExecuteDemandCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['id', 'response', 'manager']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}缺失或不能为空")
        if not isinstance(data['id'], int):
            return JsonResponse(code="999996", msg="参数格式错误")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'response', 'manager'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="执行用例id"),
            'response': openapi.Schema(type=openapi.TYPE_STRING, description="测试结果"),
            'result': openapi.Schema(type=openapi.TYPE_STRING, description="实际情况"),
            'manager': openapi.Schema(type=openapi.TYPE_STRING, description="执行人"),

        },
    ))
    def post(self, request):
        """
        执行需求测试用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        cid = data.get('id')
        case_obj = CaseInfo.objects.filter(id=cid, status=True)
        if not case_obj:
            return JsonResponse(code="999996", msg=f"id为{cid}的需求不存在或已删除")
        data['case_id'] = cid
        data['createTime'] = datetime.now()
        data['demand_id'] = case_obj[0].demand_id
        with transaction.atomic():  # 执行错误后，帮助事务回滚
            serialize = CaseExecuteRecordDeserializer(data=data)
            if serialize.is_valid():
                serialize.save()
                return JsonResponse(code="999999", msg=f"用例执行成功!", data=dict(result=result))
            else:
                return JsonResponse(code="999996", msg="用例执行成功!", data=serialize.errors)


class ExecuteDemandCaseRecordList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="caseId", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="执行用例名称id"),
    ])
    def get(self, request):
        """
        需求测试用例记录
        """
        cid = request.GET.get("caseId")
        if not cid:
            return JsonResponse(code="999996", msg="参数caseId缺失或为空!")
        case_obj = CaseInfo.objects.filter(id=cid, status=True)
        if not case_obj:
            return JsonResponse(code="999996", msg=f"id为{cid}的需求不存在或已删除")
        obj = CaseExecuteRecord.objects.filter(case_id=case_obj[0].id).order_by('-createTime')
        serialize = CaseExecuteRecordSerializer(obj, many=True)
        return JsonResponse(data={"data": serialize.data}, code="999999", msg="查询成功!")


class UploadCaseExcel(APIView):
    parser_classes = (MultiPartParser, FormParser)
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data, demand_id):
        """
        校验参数
        """
        # 校验data
        if not data or not demand_id:
            return JsonResponse(code="999996", msg="参数有误!")

    @staticmethod
    def update_case_id(demand, case_id):
        Demand.objects.filter(id=demand).update(case_id=case_id)

    @catch_exception
    def post(self, request):
        data = request.FILES.get('file')
        demand_id = request.data.get('id')
        module = request.data.get('module', '租号玩')
        start = int(request.data.get('start', 1))
        wb = xlrd2.open_workbook(filename=None, file_contents=data.read())
        table = wb.sheets()[0]
        rows = table.nrows
        num = rows + 1 - start
        file_name = data.name
        desc = request.data.get('desc', file_name.split('.xlsx')[0])
        result = self.parameter_check(data, demand_id)
        if not data.name.endswith('xlsx'):
            return JsonResponse(code="999995", msg="用例模板格式错误,应为'xlsx'格式")
        if result:
            return result
        try:
            demand = Demand.objects.get(id=demand_id)
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg=f"需求不存在！")
        params = {'name': file_name, 'file': data, 'count': num, 'desc': desc,
                  'module': module}
        if demand.case_id:
            case = LoadCaseFile.objects.filter(id=demand.case_id)
            file_full_name = (os.path.join(settings.MEDIA_ROOT, case[0].file.__dict__.get('name')))
            param_serialize = LoadCaseFileDeSerializer(data=params)
            if param_serialize.is_valid():
                if os.path.isfile(file_full_name):
                    os.remove(file_full_name)
                param_serialize.update(instance=case[0], validated_data=params)
                self.update_case_id(demand_id, case[0].id)
                return JsonResponse(code="999999", msg="用例上传成功")
            else:
                return JsonResponse(code="999995", msg="用例上传失败", data=param_serialize.errors)
        else:

            file_serializer = LoadCaseFileDeSerializer(data=params)
            if file_serializer.is_valid():
                file_serializer.save()
                self.update_case_id(demand_id, file_serializer.data.get('id'))
                return JsonResponse(code="999999", msg="用例上传成功")
            else:
                return JsonResponse(code="999995", msg="用例上传失败", data=file_serializer.errors)


class DownloadCaseFile(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="需求id"),
        openapi.Parameter(name="caseId", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="用例id")

    ])
    def get(self, request):
        """
        下载测试用例
        """
        demand_id = request.GET.get("id")
        case = request.GET.get("caseId")
        if demand_id:
            demand = Demand.objects.filter(id=demand_id)
            if not demand:
                return JsonResponse(code="999995", msg="需求不存在!")
            if not demand.filter(state=True):
                return JsonResponse(code="999995", msg="需求已删除!")
            if not demand[0].case:
                return JsonResponse(code="999995", msg="需求未上传测试用例!")
            case_id = demand[0].case_id
            if case:
                if case_id != case:
                    return JsonResponse(code="999995", msg="需求对应case_id与下载case_id不一致!")
        else:
            case_id = case
        file_load = LoadCaseFile.objects.filter(id=case_id).first()
        if not file_load:
            return JsonResponse(code="999995", msg=f"序号{case_id}测试用例不存在!")
        file = open(file_load.file.path, 'rb')
        file_name = file_load.file.name.split("/")[1]
        response = FileResponse(file)
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'attachment;filename="%s"' % file_name
        return response


class DemandCaseFileListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="module", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="所属模块"),

    ])
    def get(self, request):
        """
        获取需求用例列表
        """
        module = request.GET.get('module')
        name = request.GET.get('name')
        obj = LoadCaseFile.objects.all()
        if module:
            obj = obj.filter(module__contains=module.strip())
        if name:
            obj = obj.filter(name__contains=name.strip())
        page_size = int(request.GET.get("page_size", 10))
        page = int(request.GET.get("page", 1))
        obi = obj.order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        page_count = paginator.num_pages  # 总页数
        total = paginator.count  # 总数
        try:
            obm = paginator.page(page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = LoadCaseFileSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data, "page_sizes": page_size, "total": total,
                                  'current_page': page, 'page_count': page_count},
                            code="999999", msg="查询成功!")
