import json
import logging
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import GameInfo, SysConfig
from api_test.serializers import GameSerializer, SysConfigDeserializer, SysConfigSerializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class GameList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[])
    def get(self, request):
        """
        获取游戏列表
        """
        try:
            game_obj = GameInfo.objects.all()
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="游戏列表不存在!")

        serialize = GameSerializer(game_obj, many=True)
        return JsonResponse(data={"gameList": serialize.data}, code="999999", msg="成功!")


class SysConfigListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name='page', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description='页数'),
        openapi.Parameter(name='page_size', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description='一页显示个数'),
        openapi.Parameter(name='key', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING,
                          description='配置key'),
    ])
    def get(self, request):
        """
        获取后台配置列表
        """
        page_size = int(request.GET.get("page_size", 20))
        page = int(request.GET.get("page", 1))
        key = request.GET.get("key")
        obi = SysConfig.objects.all()
        if key:
            obi = obi.filter(key=key)
        obi = obi.order_by("id")

        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.num_pages  # 总页数
        try:
            obm = paginator.page(page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = SysConfigSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": page,
                                  "total": total
                                  }, code="999999", msg="成功！")


class AddSysConfigView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    @catch_exception
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 校验必传参数 data可以为空
            if data.get("value") and not isinstance(data["value"], str):  # 将前端传来的dict类型转为str类型
                data["value"] = json.dumps(data["value"])
            else:
                return JsonResponse(code="999996", msg="参数value不能为空!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['key', 'value'],
        properties={
            'key': openapi.Schema(type=openapi.TYPE_STRING, description="配置key"),
            'value': openapi.Schema(type=openapi.TYPE_OBJECT, description="配置value"),
            'desc': openapi.Schema(type=openapi.TYPE_STRING, description="配置描述")
        },
    ))
    def post(self, request):
        """
        新增后台配置项
        """
        data = JSONParser().parse(request)
        res = self.parameter_check(data)
        if res:
            return res
        key, value, desc = data.get('key'), data.get('value'), data.get('desc', None)
        count = SysConfig.objects.filter(key=key).count()
        if count > 0:
            return JsonResponse(code="999997", msg=f"配置{key}已存在!")
        else:
            with transaction.atomic():  # 执行错误后，帮助事务回滚
                serialize = SysConfigDeserializer(data=data)
                if serialize.is_valid():
                    serialize.save()
                    return JsonResponse(code="999999", msg=f"配置项:{key}新增成功!",
                                        data={"case_id": serialize.data.get("id")})
                else:
                    return JsonResponse(code="999996", msg="参数有误!", data=serialize.errors)
