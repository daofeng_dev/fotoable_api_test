import calendar
import logging
from datetime import datetime

from django.db.models import Q
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import Demand
from api_test.serializers import DemandSerializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class GanttView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name='start', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description='开始日期'),
        openapi.Parameter(name='end', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING,
                          description='结束日期')
    ])
    @catch_exception
    def get(self, request):
        """
        获取指定范围内的甘特图需求数据
        """
        now = datetime.now()
        start_month = request.GET.get("start", datetime(now.year, now.month, 1))
        end_month = request.GET.get("end", datetime(now.year, now.month, calendar.monthrange(now.year, now.month)[1]))
        start = request.GET.get("start", start_month)
        end = request.GET.get("end", end_month)
        demand = Demand.objects.filter(Q(startTime__range=(start, end)) | Q(
            endTime__range=(start, end)), state=True)
        serialize = DemandSerializer(demand, many=True)
        gantt_data = []
        for data in serialize.data:
            if not data.get('realTime'):
                duration = float(data.get('estimateTime')) if data.get('estimateTime') else 5
            else:
                duration = float(data.get('realTime'))
            gantt_data.append(dict(id=data['id'], start_date=data['startTime'], text=data['name'], duration=duration,
                              description=data['description']))
        return JsonResponse(data={"data": gantt_data}, code="999999", msg="成功!")
