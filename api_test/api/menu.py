import json
import logging

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from api_test.common.api_response import JsonResponse
from api_test.models import Menu, UserMenu, User
from api_test.serializers import MenuSerializer, UserMenuSerializer, MenuDeserializer, UserMenuDeserializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class MenuView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    def get(self, request):
        """
        获取前端路由
        """
        page_size = int(request.GET.get("page_size", 10))
        current_page = int(request.GET.get("page", 1))
        obi = Menu.objects.all().order_by('id')
        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = MenuSerializer(obm, many=True)
        data = serialize.data
        for i in data:
            i['parentId'] = int(i['parentId'])
            i['id'] = int(i['id'])
            if not i.get("redirect") and not i.get("path"):
                i.pop("redirect")
                i.pop("path")
            elif not i.get("redirect"):
                i.pop("redirect")
            elif not i.get("path"):
                i.pop("path")
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": current_page,
                                  "total": total,
                                  "page_sizes": page_size,
                                  }, code="999999", msg="查询前端菜单成功")


class EditMenuView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="菜单id"),
            'parentId': openapi.Schema(type=openapi.TYPE_INTEGER, description="一级菜单Id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="菜单名称"),
            'path': openapi.Schema(type=openapi.TYPE_STRING, description="二级菜单路由地址"),
            'redirect': openapi.Schema(type=openapi.TYPE_STRING, description="一级菜单路由地址")
        },
    ))
    def post(self, request):
        """
        修改前端路由
        """
        data = JSONParser().parse(request)
        mid = data.get('id')
        obj = Menu.objects.filter(id=mid)
        name = data.get('name')
        parent_id = data.get('parentId')
        path = data.get('path')
        redirect = data.get('redirect')
        if not obj:
            return JsonResponse(code="999997", msg=f"id为{mid}的菜单不存在!")
        if obj[0].parentId == 0:
            if parent_id:
                return JsonResponse(code="999997", msg=f"一级菜单不允许修改为二级菜单")
        if name:
            count = Menu.objects.filter(name=name).exclude(id=mid).count()
            if count > 0:
                return JsonResponse(code="999997", msg="存在相同名称")
        if redirect:
            count = Menu.objects.filter(redirect=redirect).exclude(id=mid).count()
            if count > 0:
                return JsonResponse(code="999997", msg="一级菜单已存在该路由地址")
        if path:
            count = Menu.objects.filter(path=path).exclude(id=mid).count()
            if count > 0:
                return JsonResponse(code="999997", msg="二级菜单已存在该路由地址")
        if parent_id:
            parent_list = [i.get('id') for i in Menu.objects.filter(parentId=0).values('id')]
            if parent_id not in parent_list:
                return JsonResponse(code="999997", msg="请选择一级菜单!")
            else:
                if parent_id == obj[0].id:
                    return JsonResponse(code="999997", msg="一级菜单不能选择自己!")
                else:
                    with transaction.atomic():
                        obj.update(**data)
                        return JsonResponse(code="999999", msg="菜单更新成功")
        else:
            if path:
                return JsonResponse(code="999997", msg="一级菜单path应该为空!")
            if not redirect:
                return JsonResponse(code="999997", msg="一级菜单redirect不应该为空!")
            with transaction.atomic():
                obj.update(**data)
                return JsonResponse(code="999999", msg="菜单更新成功")


class UserMenuView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="用户id"),
    ])
    def get(self, request):
        """
        获取用户前端路由
        """
        user_id = request.GET.get('id')
        obi = UserMenu.objects.filter(user_id=user_id).first()
        if not obi:
            if User.objects.get(id=user_id).username == 'admin':
                obi = Menu.objects.all()
            else:
                obi = Menu.objects.filter(id__in=[1, 4, 55, 56])
            serialize = MenuSerializer(obi, many=True)
            data = serialize.data
        else:
            serialize = UserMenuSerializer(obi)
            menu_list = serialize.data.get('menu_list')
            menu_obj = Menu.objects.filter(id__in=menu_list)
            data = MenuSerializer(menu_obj, many=True).data
            for i in data:
                i['parentId'] = int(i['parentId'])
                i['id'] = int(i['id'])
                if not i.get("redirect") and not i.get("path"):
                    i.pop("redirect")
                    i.pop("path")
                elif not i.get("redirect"):
                    i.pop("redirect")
                elif not i.get("path"):
                    i.pop("path")
        return JsonResponse(code="999999", msg="查询用户路由成功", data=data)


class EditMenuStatus(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'show'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="菜单ID"),
            'show': openapi.Schema(type=openapi.TYPE_INTEGER, description="菜单隐藏状态"),
        },
    ))
    def post(self, request):
        """
        隐藏菜单状态
        """
        data = JSONParser().parse(request)
        mid = data.get('id')
        obj = Menu.objects.filter(id=mid)
        show = data.get('show')
        if not obj:
            return JsonResponse(code="999997", msg=f"id为{mid}的菜单不存在!")
        meta = json.loads(obj[0].meta)
        meta['show'] = bool(show)
        with transaction.atomic():
            obj.update(meta=json.dumps(meta, ensure_ascii=False))
            title = meta.get('title', obj[0].name)
            msg = f'菜单：{title}展示成功' if show else f'菜单：{title}隐藏成功'
            return JsonResponse(code="999999", msg=msg)


class UserPrivilegesListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_privilege_list():
        privilege_list = []
        menu_obj = Menu.objects.all()
        privilege = MenuSerializer(menu_obj, many=True).data
        privilege_id = []
        for i in privilege:
            privilege_dict = {}
            if i.get('redirect'):
                privilege_dict['title'] = i.get('meta').get('title')
                privilege_dict['key'] = i.get('id')
                privilege_id.append(i.get('id'))
                try:
                    if not i.get('meta')['show']:
                        continue
                        # privilege_dict['disableCheckbox'] = True
                    else:
                        privilege_dict['disableCheckbox'] = False
                except KeyError:
                    privilege_dict['disableCheckbox'] = False
                children = []
                menu_obj_list = Menu.objects.filter(parentId=i.get('id'))
                for child in MenuSerializer(menu_obj_list, many=True).data:
                    privilege_id.append(child.get('id'))
                    result = {}
                    try:
                        result['title'], result['key'] = child.get('meta').get('title'), child.get('id')
                        if not child.get('meta')['show']:
                            continue
                            # result['disableCheckbox'] = True
                        else:
                            result['disableCheckbox'] = False
                    except KeyError:
                        result['disableCheckbox'] = False
                    if privilege_dict["disableCheckbox"]:
                        if not result['disableCheckbox']:
                            result['disableCheckbox'] = True
                    children.append(result)
                privilege_dict['children'] = children
                privilege_list.append(privilege_dict)
        return privilege_list, privilege_id

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="用户id"),
    ])
    def get(self, request):
        """
        获取用户菜单权限列表
        """
        uid = request.GET.get('id')
        privilege_list, id_list = self.get_privilege_list()
        obi = UserMenu.objects.filter(user_id=uid).first()
        if obi:
            serialize = UserMenuSerializer(obi)
            menu_list = serialize.data.get('menu_list')
        else:
            user = User.objects.filter(id=uid).first()
            if user.username != 'admin':
                menu_list = [1, 4, 55, 56]
            else:
                menu_list = id_list
        user_privilege_list = menu_list
        return JsonResponse(code="999999", msg="查询用户路由成功", data={'privilegeList': privilege_list,
                                                                 "userPrivilegeList": user_privilege_list})


class EditUserPrivilegesView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        # 校验data
        for param in ['id', 'privileges']:
            if param not in data:
                return JsonResponse(code="999996", msg=f"参数{param}缺失!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'privileges'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="用户id"),
            'privileges': openapi.Schema(type=openapi.TYPE_OBJECT, description="菜单id列表"),
        },
    ))
    def post(self, request):
        """
        修改用户权限
        """
        data = JSONParser().parse(request)
        self.parameter_check(data)
        uid = data.get('id')
        obj = User.objects.filter(id=uid)
        privileges = data.get('privileges')
        if not obj:
            return JsonResponse(code="999997", msg=f"id为{uid}的用户不存在!")
        if not privileges:
            return JsonResponse(code="999997", msg=f"用户权限不能为空")
        menu = UserMenu.objects.filter(user_id=uid)
        if not menu:
            values = dict(user_id=uid, menu_list=privileges)
            serializer = UserMenuDeserializer(data=values)
            with transaction.atomic():
                if serializer.is_valid():
                    serializer.save(user=obj[0])
                    return JsonResponse(code="999999", msg="菜单更新成功")
                else:
                    return JsonResponse(code="999995", msg="菜单更新失败", data=serializer.errors)
        with transaction.atomic():
            menu.update(menu_list=privileges)
            return JsonResponse(code="999999", msg="菜单更新成功")
