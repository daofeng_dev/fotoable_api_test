import json
import logging

from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from django_bulk_update.helper import bulk_update
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from api_test.common.api_response import JsonResponse
from api_test.common.common import record_dynamic
from api_test.common.decorator import catch_exception
from api_test.common_exception.exceptions import ParamsTypeErrorException, ParamsMissedException, \
    ObjectNotFoundException, NoPerMissionException, ObjectIsDeletedException
from api_test.models import Project, TestPlan, TestPlanCase, ApiCaseInfo, ApiInfo
from api_test.serializers import TestPlanCaseDeserializer, TestPlanCaseSerializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


# 校验项目是否存在/是否禁用/操作员是否有权限
@catch_exception
def check_object(data, request):
    project_id = data.get('projectId')
    project = Project.objects.filter(id=project_id)
    if not project:
        raise ObjectNotFoundException('项目' + str(project_id))
    project_obj = project.filter(status=True)
    if not project_obj:
        raise ObjectIsDeletedException('项目' + str(project_id))
    if not request.user.is_superuser:
        raise NoPerMissionException(request.user.username)


class TestPlanCaseList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name='planId', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description='测试计划id'),
        openapi.Parameter(name='caseId', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description='测试计划用例id'),
        openapi.Parameter(name='page', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description='page 页数'),
        openapi.Parameter(name='page_size', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description='pagesize 一页显示个数')
    ])
    @catch_exception
    def get(self, request):
        """
        获取测试计划用例集列表
        """
        try:
            page_size = int(request.GET.get('page_size', 20))
            current_page = int(request.GET.get('page', 1))
        except (TypeError, ValueError):
            return JsonResponse(code='999985', msg='page and page_size must be integer！')
        plan_id = request.GET.get('planId')
        case_id = request.GET.get('caseId')
        if plan_id:
            if not isinstance(plan_id, int):
                plan_id = int(plan_id)
        if case_id:
            if not isinstance(case_id, int):
                case_id = int(case_id)
        if plan_id:
            plan = TestPlan.objects.filter(id=plan_id)
            if not plan:
                raise ObjectNotFoundException(f'测试计划{plan_id}')
            if not plan.filter(status=True):
                raise ObjectIsDeletedException(f'测试计划{plan_id}')
            case = TestPlanCase.objects.filter(plan_id=plan_id)

        else:
            plan_list = [plan.id for plan in TestPlan.objects.filter(status=True)]
            case = TestPlanCase.objects.filter(plan_id__in=plan_list)
        if not case or not case.filter(status=True):
            return JsonResponse(code='999999', msg='success', data={'data': [], 'current_page': current_page,
                                                                    'total': 0})
        if not case_id:
            obj = case.filter(status=True).order_by('plan_id', 'index')
            paginator = Paginator(obj, page_size)  # paginator对象
            page_sizes = paginator.num_pages  # 总页数
            total = paginator.count  # 总数
            try:
                obm = paginator.page(current_page)
            except PageNotAnInteger:
                obm = paginator.page(1)
            except EmptyPage:
                obm = paginator.page(paginator.num_pages)
            serialize = TestPlanCaseSerializer(obm, many=True)
            return JsonResponse(data={'data': serialize.data,
                                      'current_page': current_page,
                                      'total': total,
                                      'page_sizes': page_sizes,
                                      }, code='999999', msg='成功！')
        obj = case.filter(id=case_id)
        if not obj:
            raise ObjectNotFoundException(f'所有测试计划下测试用例{case_id}')
        if not obj.filter(status=True):
            raise ObjectIsDeletedException(f'测试计划{obj.plan_id}下测试用例{case_id}')
        obi = obj.filter(status=True).order_by('index')
        serialize = TestPlanCaseSerializer(obi, many=True)
        return JsonResponse(code='999999', msg='success', data={'caseList': serialize.data})


class AddTestPlanCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def parameter_check(self, data):
        """
        校验参数
        """
        for param in ['planId', 'projectId']:
            if not data.get(param):
                raise ParamsMissedException(param)
            if not isinstance(data.get(param), int):
                raise ParamsTypeErrorException(param)
        if not data.get('caseList'):
            raise ParamsMissedException('caseList')
        if not isinstance(data.get('caseList'), list):
            raise ParamsTypeErrorException('caseList')
        for caseInfo in data.get('caseList'):
            for key in ['index', 'name', 'request']:
                if not caseInfo.get(key):
                    raise ParamsTypeErrorException('caseList.' + key)
            if isinstance(caseInfo.get('request'), dict):
                caseInfo['request'] = json.dumps(caseInfo['request'])
            for param in ['responseAssert', 'globalVariable']:
                if isinstance(caseInfo.get(param), list):
                    caseInfo[param] = str(caseInfo[param])

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['projectId', 'planId', 'caseList'],
        properties={
            "projectId": openapi.Parameter(name='projectId', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                                           description='项目id'),
            'planId': openapi.Schema(type=openapi.TYPE_INTEGER, description='测试计划id'),
            'caseList': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                type=openapi.TYPE_OBJECT,
                required=['index', 'name', 'data'],
                properties={
                    'index': openapi.Schema(type=openapi.TYPE_INTEGER, description='用例执行顺序index'),
                    'name': openapi.Schema(type=openapi.TYPE_STRING, description='用例名称'),
                    'request': openapi.Schema(type=openapi.TYPE_OBJECT,
                                              required=['index', 'name', 'request'],
                                              properties={'url': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                description='测试地址'),
                                                          'body': openapi.Schema(type=openapi.TYPE_OBJECT,
                                                                                 description='接口请求参数'),
                                                          'header': openapi.Schema(type=openapi.TYPE_OBJECT,
                                                                                   description='接口请求头信息'),
                                                          'method': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                   description='接口请求方法')
                                                          }, description='请求参数'),
                    'responseAssert': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        required=['key', 'value', 'index', 'flag'],
                        properties={
                            'logic': openapi.Schema(type=openapi.TYPE_STRING, default='assert断言', description='描述'),
                            'key': openapi.Schema(type=openapi.TYPE_STRING, description='断言字段路径表达式'),
                            'index': openapi.Schema(type=openapi.TYPE_STRING, default=0, description='响应结果索引'),
                            'value': openapi.Schema(type=openapi.TYPE_STRING, description='断言值'),
                            'flag': openapi.Schema(type=openapi.TYPE_STRING, default=True,
                                                   description='是否使用,默认为True')
                        }
                    )),
                    'globalVariable': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        required=['key', 'value', 'index', 'flag'],
                        properties={
                            'logic': openapi.Schema(type=openapi.TYPE_STRING, default='json提取', description='描述'),
                            'key': openapi.Schema(type=openapi.TYPE_STRING, description='提取字段命名变量名'),
                            'index': openapi.Schema(type=openapi.TYPE_STRING, default=0, description='响应结果索引'),
                            'value': openapi.Schema(type=openapi.TYPE_STRING, description='提取字段路径表达式'),
                            'flag': openapi.Schema(type=openapi.TYPE_STRING, default=True,
                                                   description='是否使用,默认为True')
                        }
                    ))

                }
            ), description='测试用例列表'),

        },
    ))
    @catch_exception
    def post(self, request):
        """
        测试计划添加用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        obj = check_object(data, request)
        if obj:
            return obj
        project_id = data.get('projectId')
        plan_id = data.get('planId')
        name_list = [case['name'] for case in data.get('caseList')]
        plan_case = TestPlanCase.objects.filter(plan_id=plan_id, name__in=name_list)
        index_list = [case['index'] for case in data.get('caseList')]
        if len(set(index_list)) < len(index_list):
            return JsonResponse(code='999995', msg='存在重复的index,请检查!')
        if len(set(name_list)) < len(name_list):
            return JsonResponse(code='999995', msg='存在重复的name,请检查!')
        if plan_case:
            if plan_case.filter(status=True):
                return JsonResponse(code='999997', msg='已存在相同的用例名称!')
            elif plan_case.filter(status=False):
                plan_case.delete()
        if TestPlanCase.objects.filter(plan_id=plan_id, index__in=index_list, status=True):
            return JsonResponse(code='999995', msg='caseList包含已存在的index,请检查!')
        with transaction.atomic():
            serialize = TestPlanCaseDeserializer(data=data.get('caseList'), many=True)
            if serialize.is_valid():
                serialize.save(plan=TestPlan.objects.get(id=plan_id),
                               createUser=User.objects.get(id=request.user.pk),
                               updateUser=User.objects.get(id=request.user.pk))
                record_dynamic(project=project_id, _type='新增', operationObject='测试计划添加用例',
                               user=request.user.pk, data="测试计划添加用例成功")
                return JsonResponse(data={'caseList': [case['id'] for case in serialize.data]},
                                    code='999999', msg='成功！')


class UpdateTestPlanCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def parameter_check(self, data):
        """
        校验参数
        """
        for param in ['id', 'index']:
            if not data.get(param):
                raise ParamsMissedException(param)
            if not isinstance(data.get(param), int):
                raise ParamsTypeErrorException(param)
        if not data.get('name'):
            raise ParamsMissedException('name')
        if not data.get('request'):
            raise ParamsMissedException('request')
        if not isinstance(data.get('name'), str):
            raise ParamsTypeErrorException('name')
        if isinstance(data.get('request'), dict):
            data['request'] = json.dumps(data['request'])
        for key in ['responseAssert', 'variables', 'globalVariable']:
            try:
                data[key]
            except KeyError:
                data[key] = []
            if isinstance(data.get(key), list):
                data[key] = str(data[key])

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['caseId', 'projectId', 'caseInfo'],

        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description='用例id'),
            'index': openapi.Schema(type=openapi.TYPE_INTEGER, description='用例执行顺序id'),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description='用例名称'),
            'request': openapi.Schema(type=openapi.TYPE_OBJECT, description='用例请求body'),
            'header': openapi.Schema(type=openapi.TYPE_OBJECT, description='用例请求header'),
            'method': openapi.Schema(type=openapi.TYPE_STRING, description='接口请求method'),
            'responseAssert': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                type=openapi.TYPE_OBJECT,
                required=['key', 'value', 'index', 'flag'],
                properties={
                    'logic': openapi.Schema(type=openapi.TYPE_STRING, default='assert断言', description='描述'),
                    'key': openapi.Schema(type=openapi.TYPE_STRING, description='断言字段路径表达式'),
                    'index': openapi.Schema(type=openapi.TYPE_STRING, default=0, description='响应结果索引'),
                    'value': openapi.Schema(type=openapi.TYPE_STRING, description='断言值'),
                    'flag': openapi.Schema(type=openapi.TYPE_STRING, default=True,
                                           description='是否使用,默认为True')
                }
            )),
            'globalVariable': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                type=openapi.TYPE_OBJECT,
                required=['key', 'value', 'index', 'flag'],
                properties={
                    'logic': openapi.Schema(type=openapi.TYPE_STRING, default='json提取', description='描述'),
                    'key': openapi.Schema(type=openapi.TYPE_STRING, description='提取字段命名变量名'),
                    'index': openapi.Schema(type=openapi.TYPE_STRING, default=0, description='响应结果索引'),
                    'value': openapi.Schema(type=openapi.TYPE_STRING, description='提取字段路径表达式'),
                    'flag': openapi.Schema(type=openapi.TYPE_STRING, default=True,
                                           description='是否使用,默认为True')
                }
            ))
        },
    ))
    @catch_exception
    def post(self, request):
        """
        修改测试计划用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        case_id = data.get('id')
        case = TestPlanCase.objects.filter(id=case_id)
        if not case:
            raise ObjectNotFoundException(f'测试计划{case.plan_id}下测试用例{case_id}')
        if case.filter(status=False):
            raise ObjectIsDeletedException(f'测试计划{case.plan_id}下测试用例{case_id}')
        # if TestPlanCase.objects.exclude(id=case_id).filter(name=data.get('name'), plan_id=case[0].plan_id):
        #     return JsonResponse(code='999997', msg='存在相同的name!')
        if TestPlanCase.objects.exclude(id=case_id).filter(index=data.get('index'), plan_id=case[0].plan_id):
            return JsonResponse(code='999997', msg='存在相同的index!')
        serialize = TestPlanCaseDeserializer(data=data)
        if serialize.is_valid():
            serialize.update(instance=case[0], validated_data=data)
            return JsonResponse(code='999999', msg='success', data={'caseId': case_id})
        return JsonResponse(code='999995', msg=serialize.errors)


class DelTestPlanCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def parameter_check(self, data):
        """
        校验参数
        """
        for param in ['planId', 'idList']:
            if not data.get(param):
                raise ParamsMissedException(param)
        if not isinstance(data.get('planId'), int):
            raise ParamsTypeErrorException('planId')
        if not isinstance(data.get('idList'), list):
            raise ParamsTypeErrorException('idList')
        for cid in data.get('idList'):
            if not isinstance(cid, int):
                raise ParamsTypeErrorException('idList列表选项')

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['planId', 'idList'],
        properties={
            'planId': openapi.Schema(type=openapi.TYPE_INTEGER, description='测试计划id'),
            'idList': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                     description='测试用例id')
        },
    ))
    def post(self, request):
        """
        删除用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        plan_id = data.get('planId')
        plan = TestPlan.objects.filter(id=plan_id)
        if not plan:
            raise ObjectNotFoundException(f'测试计划{plan_id}')
        if not plan.filter(status=True):
            raise ObjectIsDeletedException(f'测试计划{plan_id}')
        id_list = data.get('idList')
        if len(set(id_list)) < len(id_list):
            return JsonResponse(code='999995', msg='idList存在重复的id!')
        case_list = TestPlanCase.objects.filter(plan_id=plan_id, id__in=id_list)
        if case_list.count() < len(id_list):
            case_id_list = [case.id for case in case_list]
            ids = ','.join([str(i) for i in sorted(set(id_list).difference(set(id_list) & set(case_id_list)))])
            return JsonResponse(code='999995', msg=f'测试计划{plan_id}用例id为{ids}的用例不存在,请检查!')
        with transaction.atomic():
            case_list.delete()
            last_case_list = TestPlanCase.objects.exclude(plan_id=plan_id, id__in=id_list)
            case_obj = last_case_list.filter(plan_id=plan_id).order_by('index')
            for index, case in enumerate(case_obj):
                case.index = index + 1
            bulk_update(case_obj, update_fields=['index'])
        # case_list.update(status=False, updateTime=datetime.now(), updateUser=request.user.pk)
        return JsonResponse(code='999999', msg='用例删除成功！')


class UpdatePlanCaseIndex(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def parameter_check(self, data):
        """
        校验参数
        """
        for param in ['planId', 'case']:
            if not data.get(param):
                raise ParamsMissedException(param)
        if not isinstance(data.get('planId'), int):
            raise ParamsTypeErrorException('planId')
        if not isinstance(data.get('case'), dict):
            raise ParamsTypeErrorException('case')

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['planId', 'idList'],
        properties={
            'planId': openapi.Schema(type=openapi.TYPE_INTEGER, description='测试计划id'),
            'case': openapi.Schema(type=openapi.TYPE_OBJECT, description='上/下移用例'),

        },
    ))
    def post(self, request):
        """
        更新执行顺序
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        plan_id = data.get('planId')
        plan = TestPlan.objects.filter(id=plan_id)
        if not plan:
            raise ObjectNotFoundException(f'测试计划{plan_id}')
        if not plan.filter(status=True):
            raise ObjectIsDeletedException(f'测试计划{plan_id}')
        case = data.get('case')
        plan_case = TestPlanCase.objects.get(plan_id=plan_id, id=case.get('id'))
        if case.get('type') == 'up' and plan_case.index == 1:
            return JsonResponse(code='999995', msg='执行顺序为第一位的用例不允许上移!')
        elif case.get('type') == 'down' and plan_case.index == len(TestPlanCase.objects.filter(plan_id=plan_id)):
            return JsonResponse(code='999995', msg='执行顺序为最后一位的用例不允许下移!')
        with transaction.atomic():
            new_index = plan_case.index + 1 if case.get('type') == 'down' else plan_case.index - 1
            case_other_id = TestPlanCase.objects.get(plan_id=plan_id, index=new_index).id
            plan_case_other = TestPlanCase.objects.get(plan_id=plan_id, id=case_other_id)
            plan_case_other.index = plan_case.index
            plan_case.index = new_index
            plan_case.save()
            plan_case_other.save()
            return JsonResponse(code='999999', msg='执行顺序更新成功')


class InsertPlanCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def parameter_check(self, data):
        """
        校验参数
        """
        for param in ['planId', 'caseList', 'type']:
            if not data.get(param):
                raise ParamsMissedException(param)
        if not isinstance(data.get('planId'), int):
            raise ParamsTypeErrorException('planId')
        if not isinstance(data.get('caseList'), list):
            raise ParamsTypeErrorException('caseList')

    @staticmethod
    def get_case_info_list(case_list, obj_dict):
        case_obj = ApiCaseInfo.objects.filter(id__in=set(case_list))
        case_info_list = []
        for case in case_obj:
            api = ApiInfo.objects.get(id=case.api_id)
            header = {}
            if len(eval(case.header)) > 0:
                for h in eval(case.header):
                    header[f'{h["key"]}'] = h["value"]
            case_info_list.append({'index': obj_dict.get(case.id), 'name': case.name, 'description': case.description,
                                   'request': json.dumps({'url': api.apiAddress, 'body': eval(case.data),
                                                          'header': header, 'method': api.requestType
                                                          }), 'case_id': case.id,
                                   'responseAssert': "[]",
                                   'globalVariable': "[]"
                                   })
        return case_info_list

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['planId', 'idList'],
        properties={
            'planId': openapi.Schema(type=openapi.TYPE_INTEGER, description='测试计划id'),
            # 'caseList': openapi.Schema(type=openapi.TYPE_ARRAY, description='插入用例列表'),
            'planCaseId': openapi.Schema(type=openapi.TYPE_INTEGER, description='指定用例id'),
            'type': openapi.Schema(type=openapi.TYPE_STRING, description='插入位置'),
        },
    ))
    def post(self, request):
        """
        指定用例前后新增用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        plan_id = data.get('planId')
        plan = TestPlan.objects.filter(id=plan_id)
        insert_type = data.get('type')
        if not plan:
            raise ObjectNotFoundException(f'测试计划{plan_id}')
        if not plan.filter(status=True):
            raise ObjectIsDeletedException(f'测试计划{plan_id}')
        case_id = data.get('planCaseId')
        case_list = data.get('caseList')
        max_index = TestPlanCase.objects.filter(plan_id=plan_id).order_by('index').last().index
        case_index = 0
        if not case_id:
            obj_dict = {v: k + max_index + 1 for k, v in enumerate(case_list)}
            case_info_list = self.get_case_info_list(case_list, obj_dict)
        else:
            case_index = TestPlanCase.objects.filter(plan_id=plan_id, id=case_id).first().index
            obj_dict = {v: k + case_index for k, v in enumerate(case_list)} if insert_type == 'before' \
                else {v: k + case_index + 1 for k, v in enumerate(case_list)}
            case_info_list = self.get_case_info_list(case_list, obj_dict)
        with transaction.atomic():
            if case_id:
                if insert_type == 'before':
                    old_case_list = TestPlanCase.objects.filter(index__gte=case_index).all()
                    for case in old_case_list:
                        case.index += len(case_list)
                    bulk_update(old_case_list, update_fields=['index'])
                elif insert_type == 'after' and case_index != max_index:
                    old_case_list = TestPlanCase.objects.filter(index__gt=case_index).all()
                    for case in old_case_list:
                        case.index += len(case_list)
                    bulk_update(old_case_list, update_fields=['index'])
            case_serialize = TestPlanCaseDeserializer(data=case_info_list, many=True)
            if case_serialize.is_valid():
                case_serialize.save(plan=TestPlan.objects.get(id=plan_id),
                                    createUser=User.objects.get(id=request.user.pk),
                                    updateUser=User.objects.get(id=request.user.pk))
            else:
                return JsonResponse(msg=case_serialize.errors, code='999995')
        return JsonResponse(code='999999', msg='用例插入成功！')
