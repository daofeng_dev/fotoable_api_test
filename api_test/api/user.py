from coreapi.auth import TokenAuthentication
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import parsers, renderers
from rest_framework.authtoken.models import Token
from api_test.models import User
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.views import APIView
from api_test.serializers import TokenSerializer, UserSerializer
from api_test.common.api_response import JsonResponse


class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        """
        用户登录
        """
        serializer = self.serializer_class(data=request.data,
                                           context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        # token, created = Token.objects.get_or_create(user=user)
        data = TokenSerializer(Token.objects.get(user=user)).data
        data["userphoto"] = '/file/userphoto.jpg'
        data["username"] = user.username
        data["id"] = user.id
        return JsonResponse(data=data, code="999999", msg="成功")


class UserInfo(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="当前页数"),
    ])
    def get(self, request):
        """
        用户登录
        """
        name = request.GET.get("name")
        obj = User.objects.filter(username=name).first()
        data = UserSerializer(obj).data
        info = dict(username=data['username'], avatar="/avatar2.jpg", role={"permissions": [{}]}, id=data["id"])
        return JsonResponse(data=info, code="999999", msg="成功")


class UserListView(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="当前页数"),
    ])
    def get(self, request):
        """
        用户列表
        """
        page_size = int(request.GET.get("page_size", 10))
        current_page = int(request.GET.get("page", 1))
        name = request.GET.get('username')
        obj = User.objects.all().order_by('id')
        if name:
            obj = obj.filter(username=name)
        paginator = Paginator(obj, page_size)  # paginator对象
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = UserSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": current_page,
                                  "total": total,
                                  "page_sizes": page_size,
                                  }, code="999999", msg="查询用户列表成功")


obtain_auth_token = ObtainAuthToken.as_view()
