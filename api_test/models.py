# import jsonfield
# from django_mysql.models import JSONField  # 注意：JSONField在django_mysql中
from datetime import datetime
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from tinymce.models import HTMLField


POSITION_CHOICE = (
    ('项目经理', '项目经理'),
    ('开发人员', '开发人员'),
    ('测试人员', '测试人员')
)

HTTP_CHOICE = (
    ('http', 'http'),
    ('https', 'https'),
    ('ws', 'ws'),
    ('wss', 'wss'),
)

REQUEST_TYPE_CHOICE = (
    ('POST', 'POST'),
    ('GET', 'GET'),
    ('PUT', 'PUT'),
    ('DELETE', 'DELETE')
)

REQUEST_PARAMETER_TYPE_CHOICE = (
    ('form-data', '表单(form-data)'),
    ('raw', '源数据(raw)'),
    ('Restful', 'Restful')
)

PARAMETER_TYPE_CHOICE = (
    ('text', 'text'),
    ('file', 'file')
)

HTTP_CODE_CHOICE = (
    ('101', '101'),
    ('200', '200'),
    ('404', '404'),
    ('400', '400'),
    ('502', '502'),
    ('500', '500'),
    ('302', '302'),
)

ASSERT_TYPE_CHOICE = (
    ('no_check', '不校验'),
    ('only_check_status', 'statuscode校验'),
    ('subString_check', '子串校验'),
    ('entirely_check', '完全校验'),
    ('regular_check', '正则校验'),
)

EXTRACTOR_TYPE_CHOICE = (
    ('json', 'json'),
    ('regular', 'regular'),
)

UNIT_CHOICE = (
    ('m', '分'),
    ('h', '时'),
    ('d', '天'),
    ('w', '周'),
)

RESULT_CHOICE = (
    ('PASS', '成功'),
    ('FAIL', '失败'),
)

TASK_CHOICE = (
    ('circulation', '循环'),
    ('timing', '定时'),
)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


# ==================扩展用户====================================
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='用户', related_name='user')
    position = models.CharField(max_length=11, default='', blank=True, choices=POSITION_CHOICE, verbose_name='职位')

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.position


class GameInfo(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(verbose_name='游戏名称')

    class Meta:
        verbose_name = '游戏列表'
        verbose_name_plural = ' 游戏列表管理'


class Project(models.Model):
    """
    项目表
    """
    ProtocolType = (
        ('HTTP', 'HTTP'),
        ('WEBSOCKET', 'WEBSOCKET'),
        ('TCP', 'TCP'),
        ('OTHER', 'OTHER')
    )

    StructureType = (
        ('protobuf', 'protobuf'),
        ('json', 'json'),
        ('other', 'other'),
    )

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name='项目名称')
    protocol = models.CharField(max_length=50, verbose_name='协议')  # 删掉choiecs选项
    structure = models.CharField(max_length=50, verbose_name='传输格式')
    status = models.BooleanField(default=True, verbose_name='状态')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    img = models.CharField(max_length=1024, blank=True, null=True, verbose_name='项目头像url')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    LastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, max_length=1024, verbose_name='创建人')
    game = models.ForeignKey(GameInfo, on_delete=models.CASCADE, verbose_name='游戏名称', null=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '项目'
        verbose_name_plural = '项目'


class ProjectMember(models.Model):
    """
    项目成员
    """
    CHOICES = (
        ('超级管理员', '超级管理员'),
        ('开发人员', '开发人员'),
        ('测试人员', '测试人员')
    )
    id = models.AutoField(primary_key=True)
    permissionType = models.CharField(max_length=50, verbose_name='权限角色', choices=CHOICES)
    project = models.ForeignKey(Project, related_name='member_project', on_delete=models.CASCADE, verbose_name='所属项目')
    user = models.ForeignKey(User, related_name='member_user', on_delete=models.CASCADE, verbose_name='用户')

    def __unicode__(self):
        return self.permissionType

    def __str__(self):
        return self.permissionType

    class Meta:
        verbose_name = '项目成员'
        verbose_name_plural = '项目成员'


class GlobalHost(models.Model):
    """
    host域名
    """
    id = models.AutoField(primary_key=True)
    # project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='项目')
    name = models.CharField(max_length=50, verbose_name='名称')
    host = models.CharField(max_length=1024, verbose_name='IP地址')
    port = models.IntegerField(null=True, verbose_name="端口")
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    status = models.BooleanField(default=True, verbose_name='状态')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    LastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'HOST'
        verbose_name_plural = 'HOST管理'


class CustomMethod(models.Model):
    """
    自定义方法
    """
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='项目')
    name = models.CharField(max_length=50, verbose_name='方法名')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    type = models.CharField(max_length=50, verbose_name='类型')
    dataCode = models.TextField(verbose_name='代码')
    status = models.BooleanField(default=True, verbose_name='状态')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = '自定义方法'
        verbose_name_plural = '自定义方法'


class ApiGroupLevelFirst(models.Model):
    """
    接口模块
    """
    id = models.AutoField(primary_key=True)
    # project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='项目', related_name="group",
    #                             default=1)
    name = models.CharField(max_length=50, verbose_name='接口模块名称')
    status = models.BooleanField(default=True, verbose_name='状态')
    sequence = models.IntegerField(default=0, verbose_name='排序号')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    LastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '接口模块'
        verbose_name_plural = '接口模块'


class ApiVersionLevelFirst(models.Model):
    """
    接口版本
    """
    id = models.AutoField(primary_key=True)
    # project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='项目', related_name="version")
    name = models.CharField(max_length=50, verbose_name='接口版本名称')
    status = models.BooleanField(default=True, verbose_name='状态')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    LastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '接口版本'
        verbose_name_plural = '接口版本'


class ApiStatusCode(models.Model):
    """
    接口状态码
    """
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='项目')
    code = models.IntegerField(default=0, verbose_name='状态码')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    status = models.BooleanField(default=True, verbose_name='状态')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    LastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')

    def __unicode__(self):
        return self.code

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = '状态码'
        verbose_name_plural = '状态码'


class ApiDataType(models.Model):
    """
    数据类型
    """
    id = models.AutoField(primary_key=True)
    # project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='project', related_name='project',
    #                             default=1)
    type = models.CharField(max_length=50, verbose_name='数据类型')
    complex = models.BooleanField(default=False, verbose_name='是否为复杂结构')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    status = models.BooleanField(default=True, verbose_name='状态')
    system = models.BooleanField(default=False, verbose_name='创建项目时系统自动创建类型')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    LastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')

    def __unicode__(self):
        return self.type

    def __str__(self):
        return self.type

    def to_dict(self):
        opts = self._meta
        data = {}
        for f in opts.concrete_fields:
            value = f.value_from_object(self)
            if isinstance(value, datetime):
                value = value.strftime('%Y-%m-%d %H:%M:%S')
            data[f.name] = value
        return data

    class Meta:
        verbose_name = '数据类型'
        verbose_name_plural = '数据类型'


class ApiDataStructure(models.Model):
    """
    复杂数据类型的结构组成
    """
    id = models.AutoField(primary_key=True)
    type = models.ForeignKey(ApiDataType, on_delete=models.CASCADE, verbose_name='complex_datatype_id',
                             related_name='complex_datatype_id')
    name = models.CharField(max_length=50, verbose_name='参数名称')
    type_sub = models.ForeignKey(ApiDataType, on_delete=models.CASCADE, verbose_name='complex_datatype_subid',
                                 related_name='complex_datatype_subid')
    repeated = models.BooleanField(default=False, verbose_name="是否list类型")
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    status = models.BooleanField(default=True, verbose_name='状态')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    LastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '复杂数据类型组成'
        verbose_name_plural = '复杂数据类型组成'


class ApiInfo(models.Model):
    """
    接口信息
    """
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project, related_name='api_project', on_delete=models.CASCADE, verbose_name='所属项目',
                                default=1)
    apiModule = models.ForeignKey(ApiGroupLevelFirst, null=True, blank=True, related_name='First',
                                  on_delete=models.SET_NULL, verbose_name='所属一级模块')
    apiVersion = models.ForeignKey(ApiVersionLevelFirst, null=True, blank=True, related_name='VFirst',
                                   on_delete=models.SET_NULL, verbose_name='所属一级版本')
    name = models.CharField(max_length=50, verbose_name='接口名称')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    protocol = models.CharField(max_length=50, default='http', verbose_name='协议', choices=HTTP_CHOICE)
    structure = models.CharField(max_length=50, null=True, verbose_name='通信方式')
    requestType = models.CharField(max_length=50, verbose_name='请求方式', blank=True, null=True,
                                   choices=REQUEST_TYPE_CHOICE)
    apiAddress = models.CharField(max_length=1024, verbose_name='接口地址')
    status = models.BooleanField(default=True, verbose_name='状态')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')

    userUpdate = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, max_length=50, verbose_name='更新人',
                                   related_name='ApiUpdateUser')
    extend = models.CharField(max_length=1024, default="{}", verbose_name='扩展字段')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def to_dict(self):
        opts = self._meta
        data = {}
        for f in opts.concrete_fields:
            value = f.value_from_object(self)
            if isinstance(value, datetime):
                value = value.strftime('%Y-%m-%d %H:%M:%S')
            data[f.name] = value
        return data

    class Meta:
        verbose_name = '接口'
        verbose_name_plural = '接口管理'


class ApiParameter(models.Model):
    id = models.AutoField(primary_key=True)
    api = models.ForeignKey(ApiInfo, on_delete=models.CASCADE, verbose_name="所属接口", related_name='requestParameter')
    name = models.CharField(max_length=1024, verbose_name="参数名")
    type = models.ForeignKey(ApiDataType, on_delete=models.CASCADE, max_length=1024, verbose_name='参数类型',
                             related_name='request_type')
    required = models.BooleanField(default=True, verbose_name="是否必填")
    repeated = models.BooleanField(default=False, verbose_name="是否list类型")
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name="描述")
    status = models.BooleanField(default=True, verbose_name='状态')
    extends = models.CharField(max_length=1024, default="[]", verbose_name="扩展字段")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '请求参数'
        verbose_name_plural = '请求参数管理'


class ApiResponse(models.Model):
    id = models.AutoField(primary_key=True)
    api = models.ForeignKey(ApiInfo, on_delete=models.CASCADE, verbose_name="所属接口", related_name='response')
    name = models.CharField(max_length=1024, verbose_name="参数名")
    type = models.ForeignKey(ApiDataType, on_delete=models.SET_NULL, max_length=1024, null=True, blank=True,
                             verbose_name='参数类型', related_name='response_type')
    repeated = models.BooleanField(default=False, verbose_name="是否list类型")
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name="描述")
    status = models.BooleanField(default=True, verbose_name='状态')
    extends = models.CharField(max_length=1024, default="[]", verbose_name="扩展字段")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '返回参数'
        verbose_name_plural = '返回参数管理'


class ApiCaseInfo(models.Model):
    """
    接口信息
    """
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project, related_name='case_project', on_delete=models.CASCADE, verbose_name='所属项目')
    api = models.ForeignKey(ApiInfo, related_name='api_info', on_delete=models.CASCADE, verbose_name='所属接口')
    name = models.CharField(max_length=50, verbose_name='用例名称')
    status = models.BooleanField(default=True, verbose_name='状态')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    userUpdate = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, max_length=50, verbose_name='更新人',
                                   related_name='CaseUpdateUser')
    data = models.TextField(verbose_name='用例参数')
    header = models.TextField(verbose_name='用例请求头参数', null=True, default=True)
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name="描述")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '接口用例'
        verbose_name_plural = '接口用例管理'


class APIRequestHistory(models.Model):
    """
    接口请求历史
    """
    id = models.AutoField(primary_key=True)
    api = models.ForeignKey(ApiInfo, on_delete=models.CASCADE, verbose_name='接口')
    requestTime = models.DateTimeField(auto_now_add=True, verbose_name='请求时间')
    requestType = models.CharField(max_length=50, verbose_name='请求方法')
    requestAddress = models.CharField(max_length=1024, verbose_name='请求地址')
    httpCode = models.CharField(max_length=50, verbose_name='HTTP状态')

    def __unicode__(self):
        return self.requestAddress

    class Meta:
        verbose_name = '接口请求历史'
        verbose_name_plural = '接口请求历史'


class ProjectDynamic(models.Model):
    """
    项目动态
    """
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project, related_name='dynamic_project', on_delete=models.CASCADE, verbose_name='所属项目')
    time = models.DateTimeField(max_length=128, verbose_name='操作时间')
    type = models.CharField(max_length=50, verbose_name='操作类型')
    operationObject = models.CharField(max_length=50, verbose_name='操作对象')
    user = models.ForeignKey(User, blank=True, null=True, related_name='userName',
                             on_delete=models.SET_NULL, verbose_name='操作人')
    api = models.ForeignKey(ApiInfo, blank=True, null=True, related_name='apiId',
                            on_delete=models.SET_NULL, verbose_name='接口id')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')

    def __unicode__(self):
        return self.type

    class Meta:
        verbose_name = '项目动态'
        verbose_name_plural = '项目动态'


class ApiOperationHistory(models.Model):
    """
    API操作历史
    """
    id = models.AutoField(primary_key=True)
    api = models.ForeignKey(ApiInfo, on_delete=models.CASCADE, verbose_name='接口')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, max_length=50, verbose_name='用户姓名')
    time = models.DateTimeField(auto_now_add=True, verbose_name='操作时间')
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='操作内容')

    def __unicode__(self):
        return self.description

    class Meta:
        verbose_name = '接口操作历史'
        verbose_name_plural = '接口操作历史'


class VisitorsRecord(models.Model):
    """
    访客记录
    """
    id = models.AutoField(primary_key=True)
    formattedAddress = models.CharField(max_length=1024, blank=True, null=True, verbose_name="访客地址")
    country = models.CharField(max_length=50, blank=True, null=True, verbose_name="国家")
    province = models.CharField(max_length=50, blank=True, null=True, verbose_name="省份")
    city = models.CharField(max_length=50, blank=True, null=True, verbose_name="城市")
    district = models.CharField(max_length=50, blank=True, null=True, verbose_name="县级")
    township = models.CharField(max_length=50, blank=True, null=True, verbose_name="镇")
    street = models.CharField(max_length=50, blank=True, null=True, verbose_name="街道")
    number = models.CharField(max_length=50, blank=True, null=True, verbose_name="门牌号")
    success = models.CharField(max_length=50, blank=True, null=True, verbose_name="成功")
    reason = models.CharField(max_length=1024, blank=True, null=True, verbose_name="原因")
    callTime = models.DateTimeField(auto_now_add=True, verbose_name="访问时间")

    def __unicode__(self):
        return self.formattedAddress

    class Meta:
        verbose_name = "访客"
        verbose_name_plural = "访客查看"


class LoadFile(models.Model):
    """
    文件上传下载
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name='文件名称')
    desc = models.CharField(max_length=200, verbose_name='文件备注', null=True, blank=True)
    file = models.FileField(verbose_name='文件名称', upload_to='loadfile')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    status = models.BooleanField(default=True, verbose_name='状态')
    system = models.BooleanField(default=False, verbose_name='系统模版')
    template = models.CharField(max_length=200, verbose_name='模板名称')


class TestPlan(models.Model):
    """
    测试计划
    """
    id = models.AutoField(primary_key=True)
    # project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='所属项目')
    name = models.CharField(max_length=50, verbose_name='测试计划名称')
    createUser = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="创建人",
                                   related_name="testPlanCreateUser")
    updateUser = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="更新人",
                                   related_name="testPlanUpdateUser")
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name='描述')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    updateTime = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    status = models.BooleanField(default=True, verbose_name='状态')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '测试计划'
        verbose_name_plural = '测试计划管理'


class TestPlanConf(models.Model):
    """
    测试计划配置
    """
    id = models.AutoField(primary_key=True)
    plan = models.ForeignKey(TestPlan, on_delete=models.CASCADE, verbose_name='所属计划')
    env = models.TextField(verbose_name='全局环境', null=True)
    param = models.TextField(verbose_name='全局变量', null=True)
    createUser = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="创建人",
                                   related_name="testPlanConfCreateUser")
    updateUser = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="更新人",
                                   related_name="testPlanConfUpdateUser")
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    updateTime = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    class Meta:
        verbose_name = '测试计划配置'


class TestPlanCase(models.Model):
    """
    测试计划用例集执行参数
    """
    id = models.AutoField(primary_key=True)
    plan = models.ForeignKey(TestPlan, on_delete=models.CASCADE, verbose_name='所属计划')
    name = models.CharField(max_length=50, verbose_name='测试用例名称')
    description = models.CharField(max_length=200, verbose_name='测试用例描述', null=True)
    index = models.IntegerField(null=False, verbose_name='用例执行顺序')
    case_id = models.IntegerField(null=True, verbose_name='依赖用例id')
    request = models.TextField(blank=True, null=True, verbose_name='请求参数')
    responseAssert = models.TextField(verbose_name='数据验证断言')
    globalVariable = models.TextField(verbose_name='响应结果提取变量')
    status = models.BooleanField(default=True, verbose_name='状态')
    env = models.TextField(blank=True, null=True, verbose_name='执行环境')
    updateTime = models.DateTimeField(auto_now=True, verbose_name='更新事件')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    createUser = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, max_length=50, verbose_name='更新人',
                                   related_name='createUser')
    updateUser = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="创建人",
                                   related_name="updateUser")

    class Meta:
        verbose_name = '用例接口参数'
        verbose_name_plural = '用例接口参数管理'


class TestPlanExecuteCaseList(models.Model):
    """
    测试计划执行用例集
    """
    id = models.AutoField(primary_key=True)
    service = models.TextField(verbose_name='游戏服务名称')
    api_name = models.CharField(max_length=50, verbose_name='测试用例名称')
    type = models.CharField(max_length=200, verbose_name='接口方法类型', null=True)
    is_success = models.BooleanField(verbose_name='执行状态', default=False)
    protocol = models.TextField(blank=True, null=True, verbose_name='通讯协议')
    req_json = models.TextField(verbose_name='请求参数')
    params = models.TextField(verbose_name='url请求参数', null=True)
    file = models.TextField(verbose_name='文件请求参数', null=True)
    resp_json = models.TextField(verbose_name='请求响应结果', null=True)

    class Meta:
        verbose_name = '测试计划执行用例集'
        verbose_name_plural = ' 测试计划执行用例集管理'


class TestPlanExecuteCaseReport(models.Model):
    """
    测试计划执行报告
    """
    id = models.AutoField(primary_key=True)
    name = models.TextField(verbose_name='报告名称')
    plan = models.ForeignKey(TestPlan, on_delete=models.CASCADE, verbose_name='所属计划')
    report = models.TextField(verbose_name='测试报告')
    start_time = models.DateTimeField(auto_now=True, verbose_name='开始时间')
    end_time = models.DateTimeField(auto_now=True, verbose_name='结束时间')
    state = models.TextField(verbose_name='测试结果', choices=RESULT_CHOICE)

    class Meta:
        verbose_name = '测试计划执行报告'
        verbose_name_plural = ' 测试计划执行报告管理'


class HelpDocumentInfo(models.Model):
    """
    帮助文档
    """
    help_document = HTMLField(verbose_name='帮助文档')
    status = models.BooleanField(default=True, verbose_name='状态')

    def __unicode__(self):
        return self.help_document

    class Meta:
        verbose_name = '帮助文档'
        verbose_name_plural = '帮助文档管理'


class ExecutePlanInfo(models.Model):
    """
    测试计划执行记录
    """
    id = models.AutoField(primary_key=True)
    is_running = models.BooleanField(default=False, verbose_name='状态')
    start_time = models.DateTimeField(auto_now=True, verbose_name='开始时间')
    end_time = models.DateTimeField(auto_now=True, verbose_name='结束时间')

    class Meta:
        verbose_name = '测试计划执行记录'
        verbose_name_plural = '测试计划执行记录'


class MockServiceInfo(models.Model):
    """
    mock接口信息
    """
    id = models.AutoField(primary_key=True)
    address = models.TextField(default='/', verbose_name='接口地址', null=False)
    data = models.TextField(verbose_name='mock响应结果', null=True)
    status = models.BooleanField(default=True, verbose_name='状态')
    create_time = models.DateTimeField(auto_now_add=False, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    class Meta:
        verbose_name = 'mock接口信息'
        verbose_name_plural = 'mock接口信息'


class LocustCaseList(models.Model):
    """
    Locust执行记录
    """
    id = models.AutoField(primary_key=True)
    project_id = models.TextField(default='', verbose_name='项目id', null=True)
    address = models.TextField(default='/', verbose_name='接口地址', null=False)
    create_time = models.DateTimeField(auto_now_add=False, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    name = models.CharField(max_length=50, verbose_name='测试用例名称')
    type = models.CharField(max_length=200, verbose_name='接口方法类型', null=True)
    protocol = models.TextField(blank=True, null=True, verbose_name='通讯协议')
    request = models.TextField(verbose_name='请求参数')
    file = models.TextField(verbose_name='文件请求参数', null=True)
    thread = models.TextField(verbose_name='执行线程数', null=True)
    run_time = models.TextField(verbose_name='执行时间', null=True)
    set_up = models.TextField(verbose_name='单位时间内启动线程数量', null=True)
    min_wait = models.TextField(verbose_name='最小等待时间', null=True)
    max_wait = models.TextField(verbose_name='最小等待时间', null=True)
    host = models.TextField(verbose_name='接口host', null=True)
    header = models.TextField(verbose_name='请求头信息', null=True)

    class Meta:
        verbose_name = 'locust用例详情'
        verbose_name_plural = 'locust用例详情'


class LocustRecord(models.Model):
    """
    Locust执行记录
    """
    id = models.AutoField(primary_key=True)
    locust_case_id = models.TextField(default='', verbose_name='用例id', null=True)
    project_id = models.TextField(default='', verbose_name='项目id', null=True)
    address = models.TextField(default='/', verbose_name='接口地址', null=False)
    status = models.TextField(default=True, verbose_name='执行状态')
    start_time = models.DateTimeField(auto_now_add=False, verbose_name='执行开始时间', null=True)
    end_time = models.DateTimeField(auto_now=True, verbose_name='执行结束时间', null=True)
    name = models.CharField(max_length=50, verbose_name='测试用例名称')
    type = models.CharField(max_length=200, verbose_name='接口方法类型', null=True)
    protocol = models.TextField(blank=True, null=True, verbose_name='通讯协议')
    request = models.TextField(verbose_name='请求参数')
    file = models.TextField(verbose_name='文件请求参数', null=True)
    thread = models.TextField(verbose_name='执行线程数', null=True)
    run_time = models.TextField(verbose_name='执行时间', null=True)
    set_up = models.TextField(verbose_name='单位时间内启动线程数量', null=True)
    min_wait = models.TextField(verbose_name='最小等待时间', null=True)
    max_wait = models.TextField(verbose_name='最小等待时间', null=True)
    host = models.TextField(verbose_name='接口host', null=True)
    header = models.TextField(verbose_name='请求头信息', null=True)
    report_url = models.TextField(verbose_name='报告链接', null=True)

    class Meta:
        verbose_name = 'locust执行记录'
        verbose_name_plural = 'locust执行记录'


class DemandReport(models.Model):
    """
    测试报告
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name='报告名称')
    desc = models.CharField(max_length=200, verbose_name='报告备注', null=True, blank=True)
    file = models.FileField(verbose_name='测试报告', upload_to='report')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    type = models.CharField(max_length=200, verbose_name='报告类型')
    status = models.BooleanField(default=True, verbose_name='状态')

    class Meta:
        verbose_name = '测试报告'
        verbose_name_plural = '需求测试报告'


class LoadCaseFile(models.Model):
    """
    测试用例文件上传下载
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name='用例名称')
    module = models.CharField(max_length=50, verbose_name='所属模块')
    desc = models.CharField(max_length=500, verbose_name='文件备注', null=True, blank=True)
    file = models.FileField(verbose_name='文件名称', upload_to='case')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    count = models.IntegerField(verbose_name="用例数量", default=0)

    class Meta:
        verbose_name = '测试用例文件上传下载'
        verbose_name_plural = '测试用例文件上传下载'


class Demand(models.Model):
    """
    项目需求排期
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name='项目名称')
    manager = models.CharField(max_length=50, verbose_name='项目负责人')
    status = models.CharField(max_length=50, verbose_name='执行状态')
    level = models.CharField(max_length=50, verbose_name='项目优先级', default='')
    estimateTime = models.DecimalField(null=True, max_digits=19, decimal_places=2, verbose_name='预计天数', blank=True)
    realTime = models.DecimalField(null=True,  max_digits=19, decimal_places=2, verbose_name='实际天数', blank=True)
    startTime = models.DateField(verbose_name='开始时间', auto_now=False, null=True,  blank=True)
    endTime = models.DateField(verbose_name='结束时间', auto_now=False, null=True, blank=True)
    releaseTime = models.DateField(verbose_name='上线时间', auto_now=False, null=True,  blank=True)
    state = models.BooleanField(default=True, verbose_name='项目状态')
    description = models.CharField(max_length=500, verbose_name='备注', null=True, blank=True)
    report = models.ForeignKey(DemandReport, related_name='report_info', on_delete=models.CASCADE, verbose_name='报告详情', null=True)
    case = models.ForeignKey(LoadCaseFile, related_name='case_info', on_delete=models.CASCADE, verbose_name='测试用例详情', null=True)

    class Meta:
        verbose_name = '排期需求'
        verbose_name_plural = '排期需求'


class SysConfig(models.Model):
    """
    系统配置
    """
    id = models.AutoField(primary_key=True)
    key = models.CharField(max_length=50, verbose_name='配置key')
    value = models.TextField(verbose_name='配置value')
    desc = models.CharField(max_length=500, verbose_name='备注', null=True, blank=True)
    status = models.BooleanField(default=True, verbose_name='状态')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')

    class Meta:
        verbose_name = '系统配置'
        verbose_name_plural = '系统配置'


class UserAccount(models.Model):
    """
    测试用户账号
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150, verbose_name='用户名')
    loginPwd = models.TextField(max_length=150, verbose_name='登录密码', null=True, blank=True)
    payPwd = models.TextField(max_length=150, verbose_name='支付密码', null=True, blank=True)
    owner = models.TextField(max_length=150, verbose_name='拥有人', null=True, blank=True)
    desc = models.CharField(max_length=500, verbose_name='备注', null=True, blank=True)
    identify = models.TextField(max_length=500, verbose_name='用户身份', null=True, blank=True)
    money = models.TextField(max_length=500, verbose_name='账号余额', null=True, blank=True)
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')

    class Meta:
        verbose_name = '测试用户账号'
        verbose_name_plural = '测试用户账号'


class CaseInfo(models.Model):
    """
    功能测试用例
    """
    id = models.AutoField(primary_key=True)
    demand = models.ForeignKey(Demand, on_delete=models.CASCADE, verbose_name='所属需求', null=True)
    name = models.CharField(max_length=1024, verbose_name='用例标题')
    status = models.BooleanField(default=True, verbose_name='状态')
    module = models.CharField(max_length=50, verbose_name='所属模块', null=True)
    index = models.IntegerField(null=False, verbose_name='用例执行顺序', default=0)
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    userUpdate = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, max_length=50, verbose_name='更新人',
                                   related_name='UpdateCaseUser')
    steps = models.TextField(verbose_name='测试步骤')
    level = models.TextField(verbose_name='优先级')
    client = models.TextField(verbose_name='所属客户端', null=True)
    stage = models.TextField(verbose_name='适应阶段', null=True)
    preconditions = models.TextField(verbose_name='前置条件', null=True)
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name="描述")
    keywords = models.CharField(max_length=1024, blank=True, null=True, verbose_name="关键字")
    manager = models.TextField(verbose_name='负责人', default='')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '功能测试用例'
        verbose_name_plural = '功能测试用例管理'


class CaseLibrary(models.Model):
    """
    功能测试用例库
    """
    id = models.AutoField(primary_key=True)
    name = models.TextField(verbose_name='用例标题')
    status = models.BooleanField(default=True, verbose_name='状态')
    module = models.CharField(max_length=50, verbose_name='所属模块', null=True)
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近更新')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    userUpdate = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, max_length=50, verbose_name='更新人',
                                   related_name='updateCaseLibUser')
    steps = models.TextField(verbose_name='测试步骤')
    level = models.TextField(verbose_name='优先级')
    stage = models.TextField(verbose_name='适应阶段', null=True)
    preconditions = models.TextField(verbose_name='前置条件', null=True)
    description = models.CharField(max_length=1024, blank=True, null=True, verbose_name="描述")
    keywords = models.CharField(max_length=1024, blank=True, null=True, verbose_name="关键字")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '功能测试用例'
        verbose_name_plural = '功能测试用例管理'


class CaseExecuteRecord(models.Model):
    """
    功能测试用例执行记录
    """
    id = models.AutoField(primary_key=True)
    case_id = models.CharField(max_length=1024, blank=False,  verbose_name='用例id', default='')
    response = models.CharField(max_length=50, verbose_name='执行结果')
    createTime = models.DateTimeField(auto_now=False, verbose_name='执行时间')
    manager = models.TextField(verbose_name='执行人', default='')
    result = models.CharField(max_length=1024, blank=True, null=True, verbose_name='实际情况')
    round = models.CharField(max_length=100, verbose_name='执行轮次', default='')
    env = models.CharField(max_length=100, verbose_name='执行环境', default='')
    demand_id = models.CharField(max_length=1024, blank=False,  verbose_name='需求id', default='')

    class Meta:
        verbose_name = '功能测试用例执行记录'
        verbose_name_plural = '功能测试用例执行管理'


class Menu(models.Model):
    """
    前端页面菜单
    """
    id = models.AutoField(primary_key=True)
    parentId = models.IntegerField(blank=False,  verbose_name='父键', default=0)
    name = models.TextField(verbose_name='组件名称', default='')
    component = models.CharField(max_length=1024, blank=True, null=True, verbose_name='组件')
    meta = models.CharField(max_length=100, verbose_name='', default='')
    redirect = models.CharField(max_length=100, verbose_name='路由跳转地址', default='')
    path = models.CharField(max_length=1024,  verbose_name='页面路由地址', default='')

    class Meta:
        verbose_name = '前端页面菜单路由'
        verbose_name_plural = '前端页面菜单路由'


class UserMenu(models.Model):
    """
    用户前端页面菜单
    """
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='user_id', on_delete=models.CASCADE, verbose_name='用户id')
    menu_list = models.CharField(max_length=1024, blank=False,  verbose_name='菜单id列表', default=0)

    class Meta:
        verbose_name = '用户前端页面菜单'
        verbose_name_plural = '用户前端页面菜单'


class Bug(models.Model):
    """
    禅道bug列表同步数据
    """
    id = models.AutoField(primary_key=True)
    module = models.CharField(max_length=1024, verbose_name='所属模块')
    project = models.CharField(max_length=1024, verbose_name='所属项目')
    title = models.CharField(max_length=1024, verbose_name='Bug标题')
    keyword = models.CharField(max_length=1024, verbose_name='关键字')
    severity = models.CharField(max_length=1024, verbose_name='严重程度')
    type = models.CharField(max_length=1024, verbose_name='Bug类型')
    status = models.CharField(max_length=1024, verbose_name='Bug状态')
    openedBy = models.CharField(max_length=1024, verbose_name='由谁创建')
    openedDate = models.DateTimeField(verbose_name='创建日期')
    resolution = models.CharField(max_length=1024, verbose_name='解决方案')
    resolvedDate = models.DateTimeField(verbose_name='解决日期')
    resolvedBy = models.CharField(max_length=1024, verbose_name='由谁解决')
    activatedCount = models.CharField(max_length=1024, verbose_name='激活次数')

    class Meta:
        verbose_name = '禅道bug列表同步数据'
        verbose_name_plural = '禅道bug列表同步数据'


class Devices(models.Model):
    """
    设备信息
    """
    id = models.AutoField(primary_key=True)
    desc = models.CharField(max_length=1024, verbose_name='描述')
    port = models.CharField(max_length=1024, verbose_name='appium服务端口')
    platformName = models.CharField(max_length=1024, verbose_name='系统名称')
    deviceName = models.CharField(max_length=1024, verbose_name='设备名称')
    noReset = models.BooleanField(default=True, verbose_name='是否重置')
    platformVersion = models.CharField(max_length=1024, verbose_name='系统版本')
    automationName = models.CharField(max_length=1024, verbose_name='自动化名称')
    newCommandTimeout = models.CharField(max_length=1024, verbose_name='超时时间')
    bootstrapPort = models.CharField(max_length=1024, verbose_name='设备与appium交谈端口')
    brand = models.CharField(max_length=1024, verbose_name='品牌', default='')
    model = models.CharField(max_length=1024, verbose_name='型号', default='')
    screen = models.CharField(max_length=1024, verbose_name='屏幕分辨率', default='')
    image = models.CharField(max_length=1024, verbose_name='手机图片地址', default='')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')

    class Meta:
        verbose_name = '设备信息'
        verbose_name_plural = '设备信息'


class App(models.Model):
    """
    应用信息
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=1024, verbose_name='应用名称')
    appPackage = models.CharField(max_length=1024, verbose_name='应用包名')
    appActivity = models.CharField(max_length=1024, verbose_name='起始页面')
    desc = models.CharField(max_length=1024, verbose_name='描述', default='')
    createTime = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    lastUpdateTime = models.DateTimeField(auto_now=True, verbose_name='最近修改时间')

    class Meta:
        verbose_name = '应用信息'
        verbose_name_plural = '应用信息'

    def to_dict(self):
        opts = self._meta
        data = {}
        for f in opts.concrete_fields:
            value = f.value_from_object(self)
            if isinstance(value, datetime):
                value = value.strftime('%Y-%m-%d %H:%M:%S')
            data[f.name] = value
        return data


class AppAccount(models.Model):
    """
    app账号信息
    """
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=1024, verbose_name='用户名')
    password = models.CharField(max_length=1024, verbose_name='密码')
    env = models.CharField(max_length=1024, verbose_name='所属环境', default="")
    app = models.ForeignKey(App, related_name='app_info', on_delete=models.CASCADE, verbose_name='所属app')

    class Meta:
        verbose_name = 'app账号信息'
        verbose_name_plural = 'app账号信息'


class BugAnalysis(models.Model):
    """
    禅道bug数据统计
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64, verbose_name='名称')
    type = models.CharField(max_length=64, verbose_name='统计类型')
    wfcx = models.IntegerField(blank=False, verbose_name='无法重现', default=0)
    wbyy = models.IntegerField(blank=False, verbose_name='外部原因', default=0)
    byjj = models.IntegerField(blank=False, verbose_name='不予解决', default=0)
    yjj = models.IntegerField(blank=False, verbose_name='已解决', default=0)
    yqcl = models.IntegerField(blank=False, verbose_name='延期处理', default=0)
    sjrc = models.IntegerField(blank=False, verbose_name='设计如此', default=0)
    cfbug = models.IntegerField(blank=False, verbose_name='重复bug', default=0)
    xfz = models.IntegerField(blank=False, verbose_name='修复中', default=0)
    zwxq = models.IntegerField(blank=False, verbose_name='转为需求', default=0)
    total = models.IntegerField(blank=False,  verbose_name='修复中', default=0)

    class Meta:
        verbose_name = '禅道bug数据统计'
        verbose_name_plural = '禅道bug数据统计'
