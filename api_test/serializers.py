import json
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from api_test.models import Project, ProjectDynamic, ProjectMember, GlobalHost, ApiGroupLevelFirst, \
    ApiVersionLevelFirst, ApiInfo, ApiParameter, ApiResponse, ApiStatusCode, ApiDataType, ApiCaseInfo, \
    ApiDataStructure, LoadFile, TestPlan, TestPlanCase, TestPlanExecuteCaseList, TestPlanExecuteCaseReport, \
    TestPlanConf, GameInfo, HelpDocumentInfo, MockServiceInfo, LocustCaseList, LocustRecord, Demand, DemandReport, \
    SysConfig, UserAccount, CaseLibrary, CaseInfo, CaseExecuteRecord, Menu, UserMenu, Bug, LoadCaseFile, Devices, App, \
    AppAccount, BugAnalysis


class JsonSerializer(serializers.JSONField):
    default_error_messages = {
        'invalid_json': '无效的json数据格式'
    }

    def to_representation(self, value):
        try:
            val = eval(value)
        except NameError:
            val = json.loads(value)
        return json.loads(json.dumps(val))  # 兼容json.loads不支持单引号['a'，'b']

    def to_internal_value(self, data):
        try:
            if isinstance(data, str):
                json.loads(data)
        except (TypeError, ValueError):
            self.fail('invalid_json')
        return data


class TokenSerializer(serializers.ModelSerializer):
    """
    用户信息序列化
    """
    first_name = serializers.CharField(source="user.first_name")
    last_name = serializers.CharField(source="user.last_name")
    position = serializers.CharField(source="user.user.position")
    email = serializers.CharField(source="user.email")
    date_joined = serializers.CharField(source="user.date_joined")

    class Meta:
        model = Token
        fields = ('first_name', 'last_name', 'position', 'email', 'key', 'date_joined')


class UserSerializer(serializers.ModelSerializer):
    date_joined = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    last_login = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'date_joined', 'email', 'last_login')


class ProjectDeserializer(serializers.ModelSerializer):
    """
    项目信息反序列化
    """

    class Meta:
        model = Project
        fields = ('id', 'name', 'protocol', 'structure', 'status', 'LastUpdateTime', 'createTime', 'description',
                  'img', 'user', 'game')


class ProjectListSerializer(serializers.ModelSerializer):
    """
    项目信息序列化
    """

    protocol = JsonSerializer()

    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    user = serializers.CharField(source='user.username')

    class Meta:
        model = Project
        fields = ('id', 'name', 'protocol', 'structure', 'status', 'LastUpdateTime', 'createTime', 'description',
                  'img', 'user', 'game')


class ProjectSerializer(serializers.ModelSerializer):
    """
    项目信息序列化
    """

    protocol = JsonSerializer()

    apiCount = serializers.SerializerMethodField()
    caseCount = serializers.SerializerMethodField()
    dynamicCount = serializers.SerializerMethodField()
    memberCount = serializers.SerializerMethodField()
    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    user = serializers.CharField(source='user.username')
    memberList = serializers.SerializerMethodField()
    dynamicList = serializers.SerializerMethodField()
    game = serializers.CharField(source='game.name')

    class Meta:
        model = Project
        fields = ('id', 'name', 'protocol', 'structure', 'status', 'LastUpdateTime', 'createTime', 'description', 'img',
                  'apiCount', 'caseCount', 'dynamicCount', 'memberCount', 'user', 'memberList', 'dynamicList', 'game')

    def get_apiCount(self, obj):
        return obj.api_project.filter(status=True).count()

    def get_caseCount(self, obj):
        return obj.case_project.filter(status=True).count()

    def get_dynamicCount(self, obj):
        return obj.dynamic_project.all().count()

    def get_memberCount(self, obj):
        return obj.member_project.count()

    def get_memberList(self, obj):
        member_list = []
        member_project = obj.member_project.select_related('user').all()
        for index in member_project:
            member_list.append(index.user.username)
        return member_list

    def get_dynamicList(self, obj):
        dynamicList = []
        day = datetime.today() - timedelta(days=7)
        time = datetime(day.year, day.month, day.day, 0, 0, 0)
        dynamic_project = obj.dynamic_project.select_related('user'). \
            filter(time__gte=time).order_by("-id").all()
        for index in dynamic_project:
            dynamicList.append({'description': index.user.username + ' ' + index.description,
                                'time': index.time.strftime('%Y-%m-%d %H:%M:%S')})
        return dynamicList


class ProjectDynamicDeserializer(serializers.ModelSerializer):
    """
    项目动态信息反序列化
    """

    class Meta:
        model = ProjectDynamic
        fields = ('id', 'project', 'time', 'type', 'operationObject', 'user', 'api', 'description')


class ProjectDynamicSerializer(serializers.ModelSerializer):
    """
    项目动态信息序列化
    """
    operationUser = serializers.CharField(source='user.first_name')
    time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = ProjectDynamic
        fields = ('id', 'time', 'type', 'operationObject', 'operationUser', 'api', 'description')


class ProjectMemberDeserializer(serializers.ModelSerializer):
    """
    项目成员信息反序列化
    """

    class Meta:
        model = ProjectMember
        fields = ('id', 'permissionType', 'project', 'user')


class ProjectMemberSerializer(serializers.ModelSerializer):
    """
    项目成员信息序列化
    """
    username = serializers.CharField(source='user.first_name')
    userPhone = serializers.CharField(source='user.user.phone')
    userEmail = serializers.CharField(source='user.email')

    class Meta:
        model = ProjectMember
        fields = ('id', 'permissionType', 'username', 'userPhone', 'userEmail')


class GlobalHostSerializer(serializers.ModelSerializer):
    """
    host信息序列化
    """
    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = GlobalHost
        fields = ('id', 'name', 'host', 'port', 'status', 'description', 'LastUpdateTime', 'createTime')


class ApiGroupLevelFirstSerializer(serializers.ModelSerializer):
    """
    接口模块信息序列化
    """
    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    # apiInfoList = serializers.SerializerMethodField()

    class Meta:
        model = ApiGroupLevelFirst
        fields = ('id', 'name', 'sequence', 'status', 'LastUpdateTime', 'createTime', 'description')

    # def get_apiInfoList(self, obj):
    #     o = obj.First.filter(status=True)
    #     return [i.to_dict() for i in o]


class ApiGroupLevelFirstDeserializer(serializers.ModelSerializer):
    """
    接口模块信息反序列化
    """

    class Meta:
        model = ApiGroupLevelFirst
        fields = ('id', 'name', 'sequence', 'status', 'description')


class ApiVersionLevelFirstSerializer(serializers.ModelSerializer):
    """
    接口版本信息序列化
    """
    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    # apiInfoList = serializers.SerializerMethodField()

    class Meta:
        model = ApiVersionLevelFirst
        fields = ('id', 'name', 'status', 'LastUpdateTime', 'createTime', 'description')

    # def get_apiInfoList(self, obj):
    #     o = obj.VFirst.filter(status=True)
    #     return [i.to_dict() for i in o]


class ApiVersionLevelFirstDeserializer(serializers.ModelSerializer):
    """
    接口版本信息反序列化
    """

    class Meta:
        model = ApiVersionLevelFirst
        fields = ('id', 'name', 'status', 'description')


class ApiStatusCodeSerializer(serializers.ModelSerializer):
    """
    接口状态码信息序列化
    """
    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = ApiStatusCode
        fields = ('id', 'project_id', 'code', 'description', 'status', 'LastUpdateTime', 'createTime')


class ApiStatusCodeDeserializer(serializers.ModelSerializer):
    """
    接口状态码信息反序列化
    """

    class Meta:
        model = ApiStatusCode
        fields = ('id', 'project_id', 'code', 'description', 'status')


class ApiDataTypeSerializer(serializers.ModelSerializer):
    """
    接口数据类型序列化
    """
    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    class Meta:
        model = ApiDataType
        fields = ('id', 'type', 'description', 'status', 'complex', 'system', 'LastUpdateTime', 'createTime')


class ApiDataTypeDeserializer(serializers.ModelSerializer):
    """
    接口数据类型反序列化
    """

    class Meta:
        model = ApiDataType
        fields = ('id', 'type', 'description', 'status', 'system', 'complex')


class ApiDataStructureSerializer(serializers.ModelSerializer):
    """
    接口数据子类型序列化
    """
    LastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    type_sub_name = serializers.CharField(source='type_sub.type')
    type_name = serializers.CharField(source='type.type')

    class Meta:
        model = ApiDataStructure
        fields = ('id', 'name', 'repeated', 'description', 'type_id', 'type_name', 'type_sub_id', 'type_sub_name', 'status', 'LastUpdateTime', 'createTime')


class ApiDataStructureDeserializer(serializers.ModelSerializer):
    """
    接口数据子类型反序列化
    """

    class Meta:
        model = ApiDataStructure
        fields = ('id', 'name', 'repeated', 'description', 'type_id', 'type_sub_id', 'status')


class ApiParameterSerializer(serializers.ModelSerializer):
    """
    接口请求参数序列化
    """
    # type = serializers.CharField(source='type.id')

    class Meta:
        model = ApiParameter
        fields = ('id', 'api', 'name', 'type_id', 'required', 'repeated', 'description', 'status')


class ApiParameterDeserializer(serializers.ModelSerializer):
    """
    接口请求参数反序列化
    """

    class Meta:
        model = ApiParameter
        fields = ('id', 'api_id', 'name', 'type_id', 'required', 'repeated', 'description')


class ApiResponseSerializer(serializers.ModelSerializer):
    """
    接口返回参数序列化
    """

    class Meta:
        model = ApiResponse
        fields = ('id', 'api', 'name', 'type_id', 'repeated', 'description', 'status')


class ApiResponseDeserializer(serializers.ModelSerializer):
    """
    接口返回参数序列化
    """

    class Meta:
        model = ApiResponse
        fields = ('id', 'api_id', 'name', 'type_id', 'repeated', 'description', 'status')


class ApiInfoSerializer(serializers.ModelSerializer):
    """
    接口详细信息序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    requestParameter = ApiParameterSerializer(many=True, read_only=True)
    response = ApiResponseSerializer(many=True, read_only=True)
    userUpdate = serializers.CharField(source='userUpdate.username')
    # apiModule = serializers.CharField(source='apiModule.name')
    # apiVersion = serializers.CharField(source='apiVersion.name')

    requestParameterSample = serializers.SerializerMethodField()
    # responseParameterSample = serializers.SerializerMethodField()


    class Meta:
        model = ApiInfo
        fields = ('id', 'apiModule_id', 'apiVersion_id', 'name', 'protocol', 'structure', 'requestType', 'apiAddress',
                  'requestParameter', 'status', 'response', 'createTime', 'lastUpdateTime', 'userUpdate', 'description',
                  'requestParameterSample', 'project_id')

    def get_requestParameterSample(self, obj):
        return self.get_Sample(obj, "request")

    # def get_responseParameterSample(self, obj):
    #     return self.get_Sample(obj, "response")

    def get_subSample(self, obj, default_datatype_value):
        sub_data = {}
        for each in obj:
            each_sub = each.type_sub.complex_datatype_id.all()
            if not each.repeated:  # 参数不重复
                if each_sub.count() == 0:  # 非复杂类型
                    sub_data[each.name] = default_datatype_value[each.type_sub.type]
                else:  # 参数重复
                    sub_sub_data = self.get_subSample(each_sub, default_datatype_value)
                    sub_data[each.name] = sub_sub_data
            else:  # 参数重复
                if each_sub.count() == 0:  # 非复杂类型
                    sub_data[each.name] = [default_datatype_value[each.type_sub.type]]
                else:
                    sub_sub_data = self.get_subSample(each_sub, default_datatype_value)
                    sub_data[each.name] = [sub_sub_data]
        return sub_data

    def get_Sample(self, obj, flag):
        default_datatype_value = {"int": 0, "string": "string", "boolean": 0, "int32": 0, "int64": 0, "bool": 0, "long": 0, "bytes": 0}
        data = {}
        paramList = obj.requestParameter.all() if flag == 'request' else obj.response.all()
        for each in paramList:  # 遍历每个参数
            each_sub = each.type.complex_datatype_id.all()
            if not each.repeated:  # 参数非重复
                if each_sub.count() == 0:  # 非复杂类型
                    data[each.name] = default_datatype_value[each.type.type]
                else:  # 复杂类型
                    sub_data = self.get_subSample(each_sub, default_datatype_value)
                    data[each.name] = sub_data
            else:  # 参数重复
                if each_sub.count() == 0:  # 非复杂类型
                    data[each.name] = [default_datatype_value[each.type.type]]
                else:  # 复杂类型
                    sub_data = self.get_subSample(each_sub, default_datatype_value)
                    data[each.name] = [sub_data]
        return data


class ApiInfoDeserializer(serializers.ModelSerializer):
    """
    接口详细信息序列化
    """

    class Meta:
        model = ApiInfo
        fields = ('id', 'project_id', 'name', 'protocol', 'structure', 'requestType',
                  'apiAddress', 'lastUpdateTime', 'userUpdate', 'description', 'status')


class ApiCaseInfoSerializer(serializers.ModelSerializer):
    """
    接口case详细信息序列化
    """

    data = JsonSerializer()
    header = JsonSerializer()

    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    userUpdate = serializers.CharField(source='userUpdate.username')
    api = serializers.SerializerMethodField()
    # api = serializers.CharField(source='api.name')

    class Meta:
        model = ApiCaseInfo
        fields = ('id', 'api', 'name', 'data', 'header', 'createTime', 'lastUpdateTime', 'userUpdate', 'description', 'status')

    def get_api(self, obj):
        return obj.api.to_dict()


class ApiCaseInfoDeserializer(serializers.ModelSerializer):
    """
    接口case详细信息序列化
    """

    class Meta:
        model = ApiCaseInfo
        fields = ('id', 'project_id', 'api_id', 'name', 'data', 'header', 'lastUpdateTime', 'createTime', 'userUpdate', 'description', 'status')


class AutomationApiCaseInfoSerializer(serializers.ModelSerializer):
    """
    接口case详细信息序列化
    """

    data = JsonSerializer()
    header = JsonSerializer()

    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    protocol = serializers.CharField(source='api.protocol')
    structure = serializers.CharField(source='api.structure')
    requestType = serializers.CharField(source='api.requestType')
    apiAddress = serializers.CharField(source='api.apiAddress')

    class Meta:
        model = ApiCaseInfo
        fields = ('name', 'protocol', 'structure', 'requestType', 'apiAddress', 'data', 'header', 'createTime',
                  'lastUpdateTime', 'description', 'status')


class ApiInfoListSerializer(serializers.ModelSerializer):
    """
    接口信息序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    apiModule = serializers.CharField(source='apiModule.name')
    apiVersion = serializers.CharField(source='apiVersion.name')
    projectName = serializers.CharField(source='project.name')

    class Meta:
        model = ApiInfo
        fields = ('id', 'name', 'apiModule', 'apiVersion', 'createTime', 'lastUpdateTime', 'description', 'apiAddress',
                  'projectName', 'project_id')


class LoadFileSerializer(serializers.ModelSerializer):
    """
    上传-下载文件序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = LoadFile
        fields = ('id', 'name', 'file', 'createTime', 'lastUpdateTime', 'system', 'template', 'desc')


class LoadFileDeSerializer(serializers.ModelSerializer):
    """
    上传-下载文件反序列化
    """
    class Meta:
        model = LoadFile
        fields = ('id', 'name', 'file', 'createTime', 'lastUpdateTime', 'system', 'template', 'desc')


class TestPlanSerializer(serializers.ModelSerializer):
    """
    测试计划序列化
    """
    updateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createUser = serializers.CharField(source='createUser.username')
    updateUser = serializers.CharField(source='updateUser.username')

    class Meta:
        model = TestPlan
        fields = ('id', 'name', 'createUser', 'updateUser', 'description', 'createTime', 'updateTime', 'status')


class TestPlanDeserializer(serializers.ModelSerializer):
    """
    测试计划反序列化
    """
    class Meta:
        model = TestPlan
        fields = ('id', 'name', 'createUser', 'updateUser', 'description', 'createTime',
                  'updateTime', 'status')


class TestPlanCaseSerializer(serializers.ModelSerializer):
    """
    测试计划用例列表序列化
    """
    request = JsonSerializer()
    responseAssert = JsonSerializer()
    globalVariable = JsonSerializer()
    updateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createUser = serializers.CharField(source='createUser.username')
    updateUser = serializers.CharField(source='updateUser.username')

    class Meta:
        model = TestPlanCase
        fields = ('id', 'plan_id', 'name', 'description', 'status', 'responseAssert', 'case_id', 'env',
                  'globalVariable', 'index', 'request', 'createTime', 'updateTime', 'createUser', 'updateUser')


class TestPlanCaseDeserializer(serializers.ModelSerializer):
    """
    测试计划用例列表反序列化
    """
    request = JsonSerializer()

    class Meta:
        model = TestPlanCase
        fields = ('id', 'plan_id', 'name', 'description', 'status', 'responseAssert', 'case_id',
                  'globalVariable', 'index', 'request', 'env')


class TestPlanExecuteCaseListSerializer(serializers.ModelSerializer):
    """
    测试计划执行用例列表序列化
    """
    req_json = JsonSerializer()
    resp_json = JsonSerializer()

    class Meta:
        model = TestPlanExecuteCaseList
        fields = ('id', 'service', 'api_name', 'type', 'is_success', 'protocol', 'req_json', 'params',
                  'file', 'resp_json')


class TestPlanExecuteCaseListDeserializer(serializers.ModelSerializer):
    """
    测试计划执行用例列表反序列化
    """
    req_json = JsonSerializer()
    resp_json = JsonSerializer()

    class Meta:
        model = TestPlanExecuteCaseList
        fields = ('id', 'service', 'api_name', 'type', 'is_success', 'protocol', 'req_json', 'params',
                  'file', 'resp_json')


class TestPlanExecuteCaseReportSerializer(serializers.ModelSerializer):
    """
    测试计划执行用例执行报告序列化
    """
    start_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    end_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = TestPlanExecuteCaseReport
        fields = ('id', 'plan_id', 'report', 'start_time', 'name', 'end_time', 'state')


class TestPlanExecuteCaseReportListSerializer(serializers.ModelSerializer):
    """
    测试计划执行用例执行列表报告序列化
    """
    start_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    end_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = TestPlanExecuteCaseReport
        fields = ('plan_id', 'id', 'name', 'start_time', 'end_time', 'state')


class TestPlanExecuteCaseReportDeserializer(serializers.ModelSerializer):
    """
    测试计划执行用例执行报告反序列化
    """

    class Meta:
        model = TestPlanExecuteCaseReport
        fields = ('id', 'plan_id', 'report', 'start_time', 'name', 'end_time', 'state')


class TestPlanConfSerializer(serializers.ModelSerializer):
    """
    测试计划配置序列化
    """
    param = JsonSerializer()
    updateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createUser = serializers.CharField(source='createUser.username')
    updateUser = serializers.CharField(source='updateUser.username', required=False)

    class Meta:
        model = TestPlanConf
        fields = ('id', 'plan_id', 'env', 'param', 'updateTime', 'createTime', 'createUser', 'updateUser')


class TestPlanConfDeserializer(serializers.ModelSerializer):
    """
    测试计划配置反序列化
    """
    param = JsonSerializer()
    updateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createUser = serializers.CharField(source='createUser.username', required=False)
    updateUser = serializers.CharField(source='updateUser.username', required=False)

    class Meta:
        model = TestPlanConf
        fields = ('id', 'plan_id', 'env', 'param', 'updateTime', 'createTime', 'createUser', 'updateUser')


class GameSerializer(serializers.ModelSerializer):
    """
    游戏序列化
    """
    class Meta:
        model = GameInfo
        fields = ('id', 'name')


class HelpDocumentSerializer(serializers.ModelSerializer):
    """
    帮助文档序列化
    """
    class Meta:
        model = HelpDocumentInfo
        fields = ('id', 'help_document', 'status')


class MockServiceInfoSerializer(serializers.ModelSerializer):
    """
    MockService接口序列化
    """
    data = JsonSerializer()
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = MockServiceInfo
        fields = ('id', 'address', 'data', 'update_time', 'create_time', 'status')


class MockServiceInfoDeserializer(serializers.ModelSerializer):
    """
    MockService接口反序列化
    """
    data = JsonSerializer()
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = MockServiceInfo
        fields = ('id', 'address', 'data', 'status', 'update_time', 'create_time')


class LocustCaseListSerializer(serializers.ModelSerializer):
    """
    locust用例详情接口序列化
    """
    request = JsonSerializer()
    header = JsonSerializer()
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = LocustCaseList
        fields = ('id', 'address', 'name', 'update_time', 'create_time', 'type', 'protocol', 'request', 'file', 'project_id',
                  'thread', 'run_time', 'set_up', 'max_wait', 'min_wait', 'host', 'header')


class LocustCaseListDeserializer(serializers.ModelSerializer):
    """
    Locust用例详情接口反序列化
    """
    request = JsonSerializer()
    header = JsonSerializer()
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = LocustCaseList
        fields = ('id', 'address', 'name', 'update_time', 'create_time', 'type', 'protocol', 'request', 'file', 'project_id',
                  'thread', 'run_time', 'set_up', 'max_wait', 'min_wait', 'host', 'header')


class LocustRecordSerializer(serializers.ModelSerializer):
    """
    locust用例详情接口序列化
    """
    request = JsonSerializer()
    header = JsonSerializer()
    start_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    end_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = LocustRecord
        fields = ('id', 'address', 'name', 'start_time', 'end_time', 'type', 'protocol', 'request', 'file', 'project_id',
                  'thread', 'run_time', 'set_up', 'max_wait', 'min_wait', 'host', 'header', 'status', 'locust_case_id',
                  'report_url')


class LocustRecordDeserializer(serializers.ModelSerializer):
    """
    Locust用例详情接口反序列化
    """
    request = JsonSerializer()
    header = JsonSerializer()
    start_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    end_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = LocustRecord
        fields = ('id', 'address', 'name', 'start_time', 'end_time', 'type', 'protocol', 'request', 'file', 'project_id',
                  'thread', 'run_time', 'set_up', 'max_wait', 'min_wait', 'host', 'header', 'status', 'locust_case_id',

                  'report_url')


class DemandSerializer(serializers.ModelSerializer):
    """
    项目排期序列化
    """
    manager = JsonSerializer()

    class Meta:
        model = Demand
        startTime = serializers.DateField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
        dataTime = serializers.DateField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
        releaseTime = serializers.DateField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
        report = serializers.SerializerMethodField()
        case = serializers.SerializerMethodField()

        fields = ('id', 'name', 'status', 'state', 'estimateTime', 'realTime',  'releaseTime', 'manager',
                  'description', 'startTime', 'endTime', 'report', 'level', 'case')

        @staticmethod
        def get_report(obj):
            return obj.report.todict()


class DemandDeserializer(serializers.ModelSerializer):
    """
    项目排期反序列化
    """

    class Meta:
        model = Demand
        startTime = serializers.DateField(format="%Y-%m-%d", required=False, read_only=True)
        dataTime = serializers.DateField(format="%Y-%m-%d", required=False, read_only=True)
        releaseTime = serializers.DateField(format="%Y-%m-%d", required=False, read_only=True)

        fields = ('id', 'name', 'status', 'state', 'estimateTime', 'realTime',  'releaseTime', 'manager',
                  'description', 'startTime', 'endTime', 'level')


class DemandReportSerializer(serializers.ModelSerializer):
    """
    测试报告序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = DemandReport
        fields = ('id', 'name', 'createTime', 'lastUpdateTime', 'desc', 'type')


class DemandReportDeSerializer(serializers.ModelSerializer):
    """
    测试报告反序列化
    """
    class Meta:
        model = DemandReport
        fields = ('id', 'name', 'file', 'createTime', 'lastUpdateTime', 'desc', 'type')


class SysConfigSerializer(serializers.ModelSerializer):
    """
    系统配置序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    value = JsonSerializer()

    class Meta:
        model = SysConfig
        fields = ('id', 'key', 'value', 'createTime', 'lastUpdateTime', 'desc')


class SysConfigDeserializer(serializers.ModelSerializer):
    """
    系统配置反序列化
    """

    class Meta:
        model = SysConfig
        fields = ('id', 'key', 'value', 'createTime', 'lastUpdateTime', 'desc')


class UserAccountSerializer(serializers.ModelSerializer):
    """
    系统配置序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = UserAccount
        fields = ('id', 'name', 'loginPwd', 'payPwd', 'owner', 'identify', 'money', 'createTime', 'lastUpdateTime', 'desc')


class UserAccountDeserializer(serializers.ModelSerializer):
    """
    系统配置反序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = UserAccount
        fields = ('id', 'name', 'loginPwd', 'payPwd', 'owner', 'identify', 'money', 'createTime', 'lastUpdateTime', 'desc')


class CaseLibrarySerializer(serializers.ModelSerializer):
    """
    功能测试用例库序列化
    """

    # steps = JsonSerializer()
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    userUpdate = serializers.CharField(source='userUpdate.username')

    class Meta:
        model = CaseLibrary
        fields = ('id', 'name', 'status', 'module', 'createTime', 'lastUpdateTime', 'userUpdate', 'description',
                  'steps', 'level', 'stage', 'preconditions', 'keywords')


class CaseLibraryDeserializer(serializers.ModelSerializer):
    """
    功能测试用例库序列化
    """

    class Meta:
        model = CaseLibrary
        fields = ('id', 'name', 'status', 'module', 'createTime', 'lastUpdateTime', 'userUpdate', 'description',
                  'steps', 'level', 'stage', 'preconditions', 'keywords')


class CaseInfoSerializer(serializers.ModelSerializer):
    """
    需求测试用例库序列化
    """

    # steps = JsonSerializer()
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    userUpdate = serializers.CharField(source='userUpdate.username')
    demand = serializers.CharField(source='demand.name')

    class Meta:
        model = CaseInfo
        fields = ('id', 'name', 'status', 'createTime', 'lastUpdateTime', 'userUpdate', 'description', 'module',
                  'steps', 'level', 'stage', 'preconditions', 'keywords', 'client', 'demand', 'manager', 'index')


class CaseInfoDeserializer(serializers.ModelSerializer):
    """
    需求测试用例库反序列化
    """

    class Meta:
        model = CaseInfo
        fields = ('id', 'name', 'status', 'createTime', 'lastUpdateTime', 'userUpdate', 'description', 'module',
                  'steps', 'level', 'stage', 'preconditions', 'keywords', 'client', 'demand', 'manager', 'index')


class CaseExecuteRecordSerializer(serializers.ModelSerializer):
    """
    需求测试用例执行记录序列化
    """

    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = CaseExecuteRecord
        fields = ('id', 'response', 'createTime', 'case_id', 'result', 'manager', 'round', 'env', 'demand_id')


class CaseExecuteRecordDeserializer(serializers.ModelSerializer):
    """
    需求测试用例执行记录反序列化
    """

    class Meta:
        model = CaseExecuteRecord
        fields = ('id', 'response', 'createTime', 'case_id', 'result', 'manager', 'round', 'env', 'demand_id')


class MenuSerializer(serializers.ModelSerializer):
    """
    前端页面菜单管理序列化
    """
    meta = JsonSerializer()

    class Meta:
        model = Menu
        fields = ('id', 'parentId', 'meta', 'name', 'redirect', 'path', 'component')


class MenuDeserializer(serializers.ModelSerializer):
    """
    前端页面菜单管理反序列化
    """
    meta = JsonSerializer()

    class Meta:
        model = Menu
        fields = ('id', 'parentId', 'name', 'meta', 'redirect', 'path', 'component')


class UserMenuSerializer(serializers.ModelSerializer):
    """
    用户前端页面菜单管理序列化
    """
    menu_list = JsonSerializer()

    class Meta:
        model = UserMenu
        fields = ('id', 'user_id', 'menu_list')


class UserMenuDeserializer(serializers.ModelSerializer):
    """
    用户前端页面菜单管理反序列化
    """
    menu_list = JsonSerializer()

    class Meta:
        model = UserMenu
        fields = ('id', 'user_id', 'menu_list')


class BugSerializer(serializers.ModelSerializer):
    """
    禅道bug列表数据序列化
    """
    openedDate = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)
    resolvedDate = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)

    class Meta:
        model = Bug
        fields = ("id", "module", "project","title","keyword","severity","type","status","openedBy","openedDate","resolution","resolvedDate", "resolvedBy")


class BugDeserializer(serializers.ModelSerializer):
    """
    禅道bug列表数据反序列化
    """
    openedDate = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)
    resolvedDate = serializers.DateTimeField(format="%Y-%m-%d", required=False, read_only=True)

    class Meta:
        model = Bug
        fields = ("id", "module", "project", "title", "keyword", "severity", "type", "status", "openedBy", "openedDate", "resolution", "resolvedDate", "resolvedBy")


class LoadCaseFileSerializer(serializers.ModelSerializer):
    """
    测试用例上传-下载文件序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = LoadCaseFile
        fields = ('id', 'name', 'file', 'createTime', 'lastUpdateTime', 'desc', 'count', 'module')


class LoadCaseFileDeSerializer(serializers.ModelSerializer):
    """
    测试用例上传-下载文件反序列化
    """
    class Meta:
        model = LoadCaseFile
        fields = ('id', 'name', 'file', 'createTime', 'lastUpdateTime', 'desc', 'count', 'module')


class DeviceDeserializer(serializers.ModelSerializer):
    """
    设备列表数据反序列化
    """
    class Meta:
        model = Devices
        fields = ("id", "desc", "port", "platformName", "deviceName", "noReset", "platformVersion", "brand", "model",
                  "automationName", "newCommandTimeout", "bootstrapPort", "createTime", "lastUpdateTime", "screen", 'image')


class DeviceSerializer(serializers.ModelSerializer):
    """
    设备列表数据序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = Devices
        fields = ("id", "desc", "port", "platformName", "deviceName", "noReset", "platformVersion", "brand", "model",
                  "automationName", "newCommandTimeout", "bootstrapPort", "createTime", "lastUpdateTime", "screen", 'image')


class AppDeserializer(serializers.ModelSerializer):
    """
    app列表数据反序列化
    """
    class Meta:
        model = App
        fields = ("id", "name", "appPackage", "appActivity", "createTime", "lastUpdateTime", 'desc')


class AppSerializer(serializers.ModelSerializer):
    """
    app列表数据序列化
    """
    lastUpdateTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    createTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = App
        fields = ("id", "name", "appPackage", "appActivity", "createTime", "lastUpdateTime", 'desc')


class AppAccountDeserializer(serializers.ModelSerializer):
    """
    app账号列表数据反序列化
    """

    class Meta:
        model = AppAccount
        fields = ("id", "username", "password", "app", "env")


class AppAccountSerializer(serializers.ModelSerializer):
    """
    app账号列表数据序列化
    """

    class Meta:
        model = AppAccount
        fields = ("id", "username", "password", "app", "env")


class BugAnalysisSerializer(serializers.ModelSerializer):
    """
    禅道bug统计序列化
    """

    class Meta:
        model = BugAnalysis
        fields = ("id", "type", "wfcx", "wbyy", "byjj", "yjj", "yqcl", "sjrc", "cfbug", "xfz", 'zwxq', "total", "name")


class BugAnalysisDeserializer(serializers.ModelSerializer):
    """
    禅道bug统计反序列化
    """
    class Meta:
        model = BugAnalysis
        fields = ("id", "type", "wfcx", "wbyy", "byjj", "yjj", "yqcl", "sjrc", "cfbug", "xfz", 'zwxq', "total", "name")
