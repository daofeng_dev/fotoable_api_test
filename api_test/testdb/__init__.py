from api_test.testdb.test_plan_case import TestPlanCase
from api_test.testdb.test_plan_reports import TestPlanReports


class TestPlanCaseBaseLibrary(TestPlanCase, TestPlanReports):
    ROBOT_LIBRARY_SCOPE = 'GLOBAl'
