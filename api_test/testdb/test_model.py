import configparser
import os
from sqlalchemy import Column, Integer, Text, Boolean
from sqlalchemy.dialects.mysql import LONGTEXT, DATETIME
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


class TestModel(object):

    Base = declarative_base()
    metadata = Base.metadata
    config = configparser.ConfigParser()
    path = os.path.join(os.path.split(os.path.dirname(__file__))[0], 'config', 'conf.ini')
    config.read(path)
    use_local = config.getboolean('mysql', 'use_local')
    mysql_type = 'local' if use_local else 'remote'
    host = config.get('mysql', mysql_type)
    db_config = f"mysql+pymysql://root:root@{host}"
    engine = create_engine(db_config + f'/{config.get("mysql", "name")}?charset=utf8', max_overflow=5,
                           pool_size=100)
    session = sessionmaker(bind=engine)

    class TestPlanCase(Base):
        __tablename__ = 'api_test_testplanexecutecaselist'
        # __abstract__ = True
        id = Column(Integer, primary_key=True)
        service = Column(Text)
        api_name = Column(Text)
        type = Column(Text)
        is_success = Column(Boolean)
        protocol = Column(Text)
        req_json = Column(Text, default=None)
        params = Column(Text, default=None)
        file = Column(Text, default=None)
        resp_json = Column(Text, default=None)

    class TestPlanReports(Base):
        __tablename__ = 'api_test_testplanexecutecasereport'
        id = Column(Integer, primary_key=True)
        plan_id = Column(Integer)
        name = Column(Text)
        report = Column(LONGTEXT)
        start_time = Column(DATETIME)
        end_time = Column(DATETIME)
        state = Column(Text)

    def create_all_db(self):
        self.metadata.create_all(self.engine)

    def drop_all_db(self):
        self.metadata.drop_all(self.engine)
