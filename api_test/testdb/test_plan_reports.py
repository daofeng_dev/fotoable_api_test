from api_test.testdb.test_model import TestModel
from api_test.common.decorator import close_db_connection, update_db_operation


class TestPlanReports(TestModel):
    def __init__(self):
        self.session = self.session()

    @close_db_connection
    def select_all_data(self, plan_id):
        query = self.session.query(self.TestPlanReports).filter_by(plan_id=plan_id).all()
        return query

    @update_db_operation
    def add(self, plan_id, report, start, end, state):
        name = end.strftime('%Y%m%d%H%M%S')
        data = self.TestPlanReports(plan_id=plan_id, report=report, start_time=start, name=name,
                                    end_time=end, state=state)
        self.session.add(data)
