from django.conf.urls import url
from django.urls import path

from api_test.api import user, VisitorRecord
from api_test.api.account import UserAccountListView, AddUserAccountView, UpdateUserAccountView
from api_test.api.analysis import AnalysisView, DemandLevelList
from api_test.api.api import ApiList, ApiInfoDetail, AddApi, UpdateApi, DelApi, GetApiConfig
from api_test.api.app import EditAppView, AppLisView, AddAppPackageView, AppAccountLisView, AddAppAccountView
from api_test.api.batch import UploadFile, DownloadFile, FileList, DeleteFile, ImportFileApi, RunFile, UploadLibCase
from api_test.api.bug import SyncBugListView, AnalysisBugView, AnalysisBugSortedView, AnalysisBugByTypeView
from api_test.api.case import CaseInfoList, AddApiCase, UpdateApiCase, DelApiCase
from api_test.api.coverage import CoverageList, CoverageEcharts
from api_test.api.datatype import DataType, AddDataType, UpdateDataType, DelDataType, \
    DataStructure, AddDataStructure, DelDataStructure, UpdateDataStructure, DelAndAddDataStructure
from api_test.api.demand import DemandListView, AddDemandView, EditDemandView, DelDemandView, UploadDemandReport, \
    DemandReportList, DownloadReport, UpdateReportView, EditDemandStartDateView
from api_test.api.demandcase import DemandCaseList, AddDemandCase, EditDemandCase, DelDemandCase, ImportLibCase, \
    ExecuteDemandCase, ExecuteDemandCaseRecordList, UploadCaseExcel, DownloadCaseFile, DemandCaseFileListView
from api_test.api.devices import AddDeviceView, DeviceLisView, EditDeviceView, GetDesiredCapsView, OpenStfLogin, \
    GetDeviceInfoView
from api_test.api.functionaltest import FunctionalCaseLibList, AddLibraryCase, EditLibraryCase, DelLibraryCase
from api_test.api.game import GameList, SysConfigListView, AddSysConfigView
from api_test.api.gantt import GanttView
from api_test.api.group import Group, AddGroup, UpdateNameGroup, DelGroup
from api_test.api.helpdocument import HelpDocument
from api_test.api.host import HostTotal, AddHost, UpdateHost, DelHost
from api_test.api.jenkins import JenkinsJobList, ExecuteJenkinsJobList, GetJenkinsBuildsList
from api_test.api.locusttest import LocustTest, AddLocustCase, LocustRecordListView, \
    LocustCaseListView, DelLocustCaseView, UpdateLocustCaseView, DelLocustRecordView, CancelLocustTest
from api_test.api.menu import MenuView, UserMenuView, EditMenuView, EditMenuStatus, \
    UserPrivilegesListView, EditUserPrivilegesView
from api_test.api.mock import GetMockRequest, AddMockRequest, MockList, EditMockRequest, DeleteMockRequest
from api_test.api.plancase import AddTestPlanCase, TestPlanCaseList, UpdateTestPlanCase, DelTestPlanCase, \
    UpdatePlanCaseIndex, InsertPlanCase
from api_test.api.project import ProjectList, ProjectInfo, AddProject, DelProject, UpdateProject
from api_test.api.statuscode import StatusCode, AddStatusCode, UpdateStatusCode, DelStatusCode, ImportFileStatusCode
from api_test.api.testPlan import TestPlanList, AddTestPlan, UpdateTestPlan, DelTestPlan, ExecuteTestPlan, \
    ImportPlanCase, TestPlanGlobalParameter, TestPlanReportList, AddTestPlanConfig, UpdateTestPlanConfig, \
    GetTestPlanConfig, DeleteReport
from api_test.api.user import UserListView
from api_test.api.version import Version, AddVersion, UpdateNameVersion, DelVersion

urlpatterns = [
    url(r'project/project_list', ProjectList.as_view()),
    url(r'project/project_add', AddProject.as_view()),
    url(r'project/project_update', UpdateProject.as_view()),
    url(r'project/project_del', DelProject.as_view()),
    url(r'project/project_info', ProjectInfo.as_view()),

    url(r'global/host_total', HostTotal.as_view()),
    url(r'global/host_add', AddHost.as_view()),
    url(r'global/host_update', UpdateHost.as_view()),
    url(r'global/host_del', DelHost.as_view()),

    url(r'api/group_total', Group.as_view()),
    url(r'api/group_add', AddGroup.as_view()),
    url(r'api/group_update', UpdateNameGroup.as_view()),
    url(r'api/group_del', DelGroup.as_view()),

    url(r'api/version_total', Version.as_view()),
    url(r'api/version_add', AddVersion.as_view()),
    url(r'api/version_update', UpdateNameVersion.as_view()),
    url(r'api/version_del', DelVersion.as_view()),

    url(r'api/statuscode_total', StatusCode.as_view()),
    url(r'api/statuscode_add', AddStatusCode.as_view()),
    url(r'api/statuscode_update', UpdateStatusCode.as_view()),
    url(r'api/statuscode_del', DelStatusCode.as_view()),

    url(r'global/datatype_total', DataType.as_view()),
    url(r'global/datatype_add', AddDataType.as_view()),
    url(r'global/datatype_update', UpdateDataType.as_view()),
    url(r'global/datatype_del', DelDataType.as_view()),
    url(r'global/datastructure_batch_update', DelAndAddDataStructure.as_view()),



    url(r'global/datastructure_total', DataStructure.as_view()),
    url(r'global/datastructure_add', AddDataStructure.as_view()),
    url(r'global/datastructure_update', UpdateDataStructure.as_view()),
    url(r'global/datastructure_del', DelDataStructure.as_view()),

    url(r'api/api_list', ApiList.as_view()),
    url(r'api/api_add', AddApi.as_view()),
    url(r'api/api_update', UpdateApi.as_view()),
    url(r'api/api_del', DelApi.as_view()),
    url(r'api/api_info', ApiInfoDetail.as_view()),
    url(r'api/config', GetApiConfig.as_view()),

    url(r'case/case_info', CaseInfoList.as_view()),
    url(r'case/case_add', AddApiCase.as_view()),
    url(r'case/case_update', UpdateApiCase.as_view()),
    url(r'case/case_del', DelApiCase.as_view()),

    url(r'user/login', user.obtain_auth_token),
    url(r'user/info', user.UserInfo.as_view()),
    url(r'user/privileges', UserPrivilegesListView.as_view()),
    url(r'user/list', UserListView.as_view()),
    url(r'user/editPrivileges', EditUserPrivilegesView.as_view()),



    url(r'user/VisitorRecord', VisitorRecord.Record.as_view()),

    url(r'file/List', FileList.as_view()),
    url(r'file/Upload', UploadFile.as_view()),
    url(r'file/Download', DownloadFile.as_view()),
    url(r'file/Delete', DeleteFile.as_view()),
    url(r'file/Run', RunFile.as_view()),
    url(r'file/api_import', ImportFileApi.as_view()),
    url(r'file/statusCode_import', ImportFileStatusCode.as_view()),

    url(r'plan/list', TestPlanList.as_view()),
    url(r'plan/add', AddTestPlan.as_view()),
    url(r'plan/edit', UpdateTestPlan.as_view()),
    url(r'plan/delete', DelTestPlan.as_view()),
    url(r'plan/execute', ExecuteTestPlan.as_view()),
    url(r'plan/params', TestPlanGlobalParameter.as_view()),
    url(r'plan/reports', TestPlanReportList.as_view()),
    url(r'planReports/delete', DeleteReport.as_view()),
    #
    url(r'planConf/add', AddTestPlanConfig.as_view()),
    url(r'planConf/edit', UpdateTestPlanConfig.as_view()),
    url(r'planConf/list', GetTestPlanConfig.as_view()),

    #
    url(r'planCase/add', AddTestPlanCase.as_view()),
    url(r'planCase/list', TestPlanCaseList.as_view()),
    url(r'planCase/edit', UpdateTestPlanCase.as_view()),
    url(r'planCase/import', ImportPlanCase.as_view()),
    url(r'planCase/delete', DelTestPlanCase.as_view()),
    url(r'planCase/index/edit', UpdatePlanCaseIndex.as_view()),
    url(r'planCase/insert', InsertPlanCase.as_view()),

    #
    url(r'game/list', GameList.as_view()),

    url(r'helpDocument', HelpDocument.as_view()),
    path('mock/<path:address>', GetMockRequest.as_view()),
    url(r'mocker/add', AddMockRequest.as_view()),
    url(r'mocker/list', MockList.as_view()),
    url(r'mocker/edit', EditMockRequest.as_view()),
    url(r'mocker/delete', DeleteMockRequest.as_view()),

    url(r'coverage/list', CoverageList.as_view()),
    url(r'coverage/echarts', CoverageEcharts.as_view()),
    url(r'locust/run', LocustTest.as_view()),
    url(r'locustCase/add', AddLocustCase.as_view()),
    url(r'locustRecord/list', LocustRecordListView.as_view()),
    url(r'locustCase/list', LocustCaseListView.as_view()),
    url(r'locustCase/edit', UpdateLocustCaseView.as_view()),
    url(r'locustCase/delete', DelLocustCaseView.as_view()),
    url(r'locustRecord/delete', DelLocustRecordView.as_view()),
    url(r'locust/cancel', CancelLocustTest.as_view()),
    url(r'jenkinsJob/list', JenkinsJobList.as_view()),
    url(r'jenkinsJob/execute', ExecuteJenkinsJobList.as_view()),
    url(r'jenkinsJob/buildList', GetJenkinsBuildsList.as_view()),
    url(r'demand/add', AddDemandView.as_view()),
    url(r'demand/list', DemandListView.as_view()),
    url(r'demand/edit', EditDemandView.as_view()),
    url(r'demandDate/edit', EditDemandStartDateView.as_view()),
    url(r'demand/delete', DelDemandView.as_view()),
    url(r'demandReport/upload', UploadDemandReport.as_view()),
    url(r'demandReport/list', DemandReportList.as_view()),
    url(r'demandReport/edit', UpdateReportView.as_view()),
    url(r'demandReport/download', DownloadReport.as_view()),
    url(r'analysis/info', AnalysisView.as_view()),
    url(r'analysis/levelList', DemandLevelList.as_view()),
    url(r'sysConf/list', SysConfigListView.as_view()),
    url(r'sysConf/add', AddSysConfigView.as_view()),
    url(r'gantt/list', GanttView.as_view()),
    url(r'account/list', UserAccountListView.as_view()),
    url(r'account/add', AddUserAccountView.as_view()),
    url(r'account/edit', UpdateUserAccountView.as_view()),
    url(r'caseLibrary/list', FunctionalCaseLibList.as_view()),
    url(r'caseLibrary/add', AddLibraryCase.as_view()),
    url(r'caseLibrary/edit', EditLibraryCase.as_view()),
    url(r'caseLibrary/delete', DelLibraryCase.as_view()),
    url(r'caseLibrary/import', UploadLibCase.as_view()),
    url(r'demandCase/list', DemandCaseList.as_view()),
    url(r'demandCase/add', AddDemandCase.as_view()),
    url(r'demandCase/edit', EditDemandCase.as_view()),
    url(r'demandCase/demand', DelDemandCase.as_view()),
    url(r'demandCase/import', ImportLibCase.as_view()),
    url(r'demandCase/execute', ExecuteDemandCase.as_view()),
    url(r'demandCaseRecord/list', ExecuteDemandCaseRecordList.as_view()),
    url(r'demandCase/fileList', DemandCaseFileListView.as_view()),
    url(r'menu/list', MenuView.as_view()),
    url(r'userMenu/list', UserMenuView.as_view()),
    url(r'menu/edit', EditMenuView.as_view()),
    url(r'menu/status', EditMenuStatus.as_view()),
    url(r'bug/sync', SyncBugListView.as_view()),
    url(r'bug/analysis', AnalysisBugView.as_view()),
    url(r'bugSorted/analysis', AnalysisBugSortedView.as_view()),
    url(r'bugSync/analysis', AnalysisBugByTypeView.as_view()),
    url(r'upload/case', UploadCaseExcel.as_view()),
    url(r'demandCase/download', DownloadCaseFile.as_view()),
    url(r'device/add', AddDeviceView.as_view()),
    url(r'device/edit', EditDeviceView.as_view()),
    url(r'device/list', DeviceLisView.as_view()),
    url(r'app/add', AddAppPackageView.as_view()),
    url(r'app/edit', EditAppView.as_view()),
    url(r'app/list', AppLisView.as_view()),
    url(r'appAccount/list', AppAccountLisView.as_view()),
    url(r'appAccount/add', AddAppAccountView.as_view()),
    url(r'desired/caps', GetDesiredCapsView.as_view()),
    url(r'openStf/mockLogin', OpenStfLogin.as_view()),
    url(r'openStf/deviceInfo', GetDeviceInfoView.as_view())



]
